package com.bakaeva.tm.repository;

import com.bakaeva.tm.api.repository.IUserRepository;
import com.bakaeva.tm.dto.UserDTO;
import com.bakaeva.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.bakaeva.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @Test
    public void add() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1);
        Assert.assertEquals(USER1, repository.findAll().get(0));
    }

    @Test
    public void addAll() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(USER_LIST);
        Assert.assertEquals(USER_LIST, repository.findAll());
    }

    @Test
    public void remove() {
        @NotNull final List<UserDTO> list = new ArrayList<>(USER_LIST);
        @NotNull final IUserRepository repository = new UserRepository();
        repository.addAll(USER_LIST);
        @Nullable final UserDTO removed = repository.remove(USER2);
        Assert.assertEquals(USER2, removed);
        list.remove(USER2);
        Assert.assertEquals(list, repository.findAll());
    }

    @Test
    public void load() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(ADMIN1);
        repository.load(USER_LIST);
        Assert.assertEquals(USER_LIST, repository.findAll());
    }

    @Test
    public void clear() {
        @NotNull final IUserRepository repository = new UserRepository();
        repository.addAll(USER_LIST);
        repository.clear();
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void findAll() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(ADMIN1);
        repository.add(USER2);
        repository.add(USER1);
        Assert.assertEquals(
                Arrays.asList(ADMIN1, USER2, USER1),
                repository.findAll()
        );
    }

    @Test
    public void findById() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(USER_LIST);
        Assert.assertEquals(
                repository.findById(USER2.getId()),
                USER2
        );
    }

    @Test
    public void findByLogin() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(USER_LIST);
        Assert.assertEquals(
                repository.findByLogin(USER2.getLogin()),
                USER2
        );
    }

    @Test
    public void removeById() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(USER_LIST);
        Assert.assertEquals(
                repository.removeById(USER2.getId()),
                USER2
        );
        @NotNull final List<UserDTO> list = new ArrayList<>(USER_LIST);
        list.remove(USER2);
        Assert.assertEquals(repository.findAll(), list);
    }

    @Test
    public void removeByLogin() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(USER_LIST);
        Assert.assertEquals(
                repository.removeByLogin(USER2.getLogin()),
                USER2
        );
        @NotNull final List<UserDTO> list = new ArrayList<>(USER_LIST);
        list.remove(USER2);
        Assert.assertEquals(repository.findAll(), list);
    }

}