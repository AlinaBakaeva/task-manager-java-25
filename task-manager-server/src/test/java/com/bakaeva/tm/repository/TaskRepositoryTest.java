package com.bakaeva.tm.repository;

import com.bakaeva.tm.api.repository.ITaskRepository;
import com.bakaeva.tm.dto.TaskDTO;
import com.bakaeva.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.ArrayList;
import java.util.List;

import static com.bakaeva.tm.constant.TaskTestData.*;
import static com.bakaeva.tm.constant.UserTestData.ADMIN1;
import static com.bakaeva.tm.constant.UserTestData.USER1;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @Test
    public void add() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_TASK1);
        Assert.assertEquals(USER1_TASK1, repository.findAll().get(0));
    }

    @Test
    public void addByUserId() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1.getId(), USER1_TASK1);
        Assert.assertEquals(USER1_TASK1, repository.findAll().get(0));
    }

    @Test
    public void addAll() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(TASK_LIST);
        Assert.assertEquals(TASK_LIST, repository.findAll());
    }

    @Test
    public void remove() {
        @NotNull final List<TaskDTO> list = new ArrayList<>(USER1_TASK_LIST);
        @NotNull final ITaskRepository repository = new TaskRepository();
        repository.addAll(USER1_TASK_LIST);
        @Nullable final TaskDTO removed = repository.remove(USER1_TASK2);
        Assert.assertEquals(USER1_TASK2, removed);
        list.remove(USER1_TASK2);
        Assert.assertEquals(list, repository.findAll());
        Assert.assertNull(repository.remove(USER1_TASK2));
    }

    @Test
    public void removeByUserId() {
        @NotNull final List<TaskDTO> list = new ArrayList<>(TASK_LIST);
        @NotNull final ITaskRepository repository = new TaskRepository();
        repository.addAll(TASK_LIST);
        repository.remove(USER1.getId(), USER1_TASK2);
        list.remove(USER1_TASK2);
        Assert.assertEquals(list, repository.findAll());

        repository.remove(USER1.getId(), ADMIN1_TASK1);
        Assert.assertTrue(TASK_LIST.contains(ADMIN1_TASK1));
        list.remove(ADMIN1_TASK1);
        Assert.assertNotEquals(list, repository.findAll());
    }

    @Test
    public void load() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(USER1_TASK_LIST);
        repository.load(TASK_LIST);
        Assert.assertEquals(TASK_LIST, repository.findAll());
    }

    @Test
    public void clear() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        repository.addAll(TASK_LIST);
        repository.clear();
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void clearByUserId() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        repository.addAll(TASK_LIST);
        repository.clear(USER1.getId());
        Assert.assertFalse(repository.findAll().containsAll(USER1_TASK_LIST));
        @NotNull final List<TaskDTO> result = new ArrayList<>(TASK_LIST);
        result.removeAll(USER1_TASK_LIST);
        Assert.assertEquals(repository.findAll(), result);
    }

    @Test
    public void findAll() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(TASK_LIST);
        Assert.assertEquals(TASK_LIST, repository.findAll());
    }

    @Test
    public void findAllByUserId() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(TASK_LIST);
        Assert.assertEquals(ADMIN1_TASK_LIST, repository.findAll(ADMIN1.getId()));
    }

    @Test
    public void findById() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(TASK_LIST);
        Assert.assertEquals(
                repository.findById(USER1.getId(), USER1_TASK2.getId()),
                USER1_TASK2
        );
        Assert.assertNull(repository.findById(USER1.getId(), ADMIN1_TASK2.getId()));
    }

    @Test
    public void findByIndex() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(TASK_LIST);
        final int index = ADMIN1_TASK_LIST.indexOf(ADMIN1_TASK2);
        Assert.assertEquals(
                repository.findByIndex(ADMIN1.getId(), index),
                ADMIN1_TASK2
        );
    }

    @Test
    public void findByName() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(TASK_LIST);
        Assert.assertEquals(
                repository.findByName(USER1.getId(), USER1_TASK2.getName()),
                USER1_TASK2
        );
    }

    @Test
    public void removeById() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(TASK_LIST);
        Assert.assertEquals(
                repository.removeById(ADMIN1.getId(), ADMIN1_TASK2.getId()),
                ADMIN1_TASK2
        );
        @NotNull final List<TaskDTO> list = new ArrayList<>(TASK_LIST);
        list.remove(ADMIN1_TASK2);
        Assert.assertEquals(repository.findAll(), list);
        Assert.assertNull(repository.removeById(USER1.getId(), ADMIN1_TASK2.getId()));
        Assert.assertEquals(repository.findAll(), list);
    }

    @Test
    public void removeByIndex() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final List<TaskDTO> list = new ArrayList<>(TASK_LIST);
        repository.addAll(TASK_LIST);
        final int index = ADMIN1_TASK_LIST.indexOf(ADMIN1_TASK2);
        Assert.assertEquals(
                repository.removeByIndex(ADMIN1.getId(), index),
                ADMIN1_TASK2
        );
        list.remove(ADMIN1_TASK2);
        Assert.assertEquals(repository.findAll(), list);
        Assert.assertNull(repository.removeById(USER1.getId(), ADMIN1_TASK2.getId()));
        Assert.assertEquals(repository.findAll(), list);
    }

    @Test
    public void removeByName() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(TASK_LIST);
        Assert.assertEquals(
                repository.removeByName(ADMIN1.getId(), ADMIN1_TASK2.getName()),
                ADMIN1_TASK2
        );
        @NotNull final List<TaskDTO> list = new ArrayList<>(TASK_LIST);
        list.remove(ADMIN1_TASK2);
        Assert.assertEquals(repository.findAll(), list);
        Assert.assertNull(repository.removeById(USER1.getId(), ADMIN1_TASK2.getName()));
        Assert.assertEquals(repository.findAll(), list);
    }

}
