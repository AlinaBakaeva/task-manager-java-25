package com.bakaeva.tm.repository;

import com.bakaeva.tm.api.repository.IProjectRepository;
import com.bakaeva.tm.dto.ProjectDTO;
import com.bakaeva.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.ArrayList;
import java.util.List;

import static com.bakaeva.tm.constant.ProjectTestData.*;
import static com.bakaeva.tm.constant.UserTestData.ADMIN1;
import static com.bakaeva.tm.constant.UserTestData.USER1;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @Test
    public void add() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, repository.findAll().get(0));
    }

    @Test
    public void addByUserId() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1.getId(), USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, repository.findAll().get(0));
    }

    @Test
    public void addAll() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(PROJECT_LIST);
        Assert.assertEquals(PROJECT_LIST, repository.findAll());
    }

    @Test
    public void remove() {
        @NotNull final List<ProjectDTO> list = new ArrayList<>(USER1_PROJECT_LIST);
        @NotNull final IProjectRepository repository = new ProjectRepository();
        repository.addAll(USER1_PROJECT_LIST);
        @Nullable final ProjectDTO removed = repository.remove(USER1_PROJECT2);
        Assert.assertEquals(USER1_PROJECT2, removed);
        list.remove(USER1_PROJECT2);
        Assert.assertEquals(list, repository.findAll());
        Assert.assertNull(repository.remove(USER1_PROJECT2));
    }

    @Test
    public void removeByUserId() {
        @NotNull final List<ProjectDTO> list = new ArrayList<>(PROJECT_LIST);
        @NotNull final IProjectRepository repository = new ProjectRepository();
        repository.addAll(PROJECT_LIST);
        repository.remove(USER1.getId(), USER1_PROJECT2);
        list.remove(USER1_PROJECT2);
        Assert.assertEquals(list, repository.findAll());

        repository.remove(USER1.getId(), ADMIN1_PROJECT1);
        Assert.assertTrue(PROJECT_LIST.contains(ADMIN1_PROJECT1));
        list.remove(ADMIN1_PROJECT1);
        Assert.assertNotEquals(list, repository.findAll());
    }

    @Test
    public void load() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(USER1_PROJECT_LIST);
        repository.load(PROJECT_LIST);
        Assert.assertEquals(PROJECT_LIST, repository.findAll());
    }

    @Test
    public void clear() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        repository.addAll(PROJECT_LIST);
        repository.clear();
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void clearByUserId() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        repository.addAll(PROJECT_LIST);
        repository.clear(USER1.getId());
        Assert.assertFalse(repository.findAll().containsAll(USER1_PROJECT_LIST));
        @NotNull final List<ProjectDTO> result = new ArrayList<>(PROJECT_LIST);
        result.removeAll(USER1_PROJECT_LIST);
        Assert.assertEquals(repository.findAll(), result);
    }

    @Test
    public void findAll() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(PROJECT_LIST);
        Assert.assertEquals(PROJECT_LIST, repository.findAll());
    }

    @Test
    public void findAllByUserId() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(PROJECT_LIST);
        Assert.assertEquals(ADMIN1_PROJECT_LIST, repository.findAll(ADMIN1.getId()));
    }

    @Test
    public void findById() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(PROJECT_LIST);
        Assert.assertEquals(
                repository.findById(USER1.getId(), USER1_PROJECT2.getId()),
                USER1_PROJECT2
        );
        Assert.assertNull(repository.findById(USER1.getId(), ADMIN1_PROJECT2.getId()));
    }

    @Test
    public void findByIndex() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(PROJECT_LIST);
        final int index = ADMIN1_PROJECT_LIST.indexOf(ADMIN1_PROJECT2);
        Assert.assertEquals(
                repository.findByIndex(ADMIN1.getId(), index),
                ADMIN1_PROJECT2
        );
    }

    @Test
    public void findByName() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(PROJECT_LIST);
        Assert.assertEquals(
                repository.findByName(USER1.getId(), USER1_PROJECT2.getName()),
                USER1_PROJECT2
        );
    }

    @Test
    public void removeById() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(PROJECT_LIST);
        Assert.assertEquals(
                repository.removeById(ADMIN1.getId(), ADMIN1_PROJECT2.getId()),
                ADMIN1_PROJECT2
        );
        @NotNull final List<ProjectDTO> list = new ArrayList<>(PROJECT_LIST);
        list.remove(ADMIN1_PROJECT2);
        Assert.assertEquals(repository.findAll(), list);
        Assert.assertNull(repository.removeById(USER1.getId(), ADMIN1_PROJECT2.getId()));
        Assert.assertEquals(repository.findAll(), list);
    }

    @Test
    public void removeByIndex() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final List<ProjectDTO> list = new ArrayList<>(PROJECT_LIST);
        repository.addAll(PROJECT_LIST);
        final int index = ADMIN1_PROJECT_LIST.indexOf(ADMIN1_PROJECT2);
        Assert.assertEquals(
                repository.removeByIndex(ADMIN1.getId(), index),
                ADMIN1_PROJECT2
        );
        list.remove(ADMIN1_PROJECT2);
        Assert.assertEquals(repository.findAll(), list);
        Assert.assertNull(repository.removeById(USER1.getId(), ADMIN1_PROJECT2.getId()));
        Assert.assertEquals(repository.findAll(), list);
    }

    @Test
    public void removeByName() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(PROJECT_LIST);
        Assert.assertEquals(
                repository.removeByName(ADMIN1.getId(), ADMIN1_PROJECT2.getName()),
                ADMIN1_PROJECT2
        );
        @NotNull final List<ProjectDTO> list = new ArrayList<>(PROJECT_LIST);
        list.remove(ADMIN1_PROJECT2);
        Assert.assertEquals(repository.findAll(), list);
        Assert.assertNull(repository.removeById(USER1.getId(), ADMIN1_PROJECT2.getName()));
        Assert.assertEquals(repository.findAll(), list);
    }


}
