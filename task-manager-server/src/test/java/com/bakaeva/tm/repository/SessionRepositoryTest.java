package com.bakaeva.tm.repository;

import com.bakaeva.tm.api.repository.ISessionRepository;
import com.bakaeva.tm.dto.SessionDTO;
import com.bakaeva.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.Arrays;
import java.util.List;

import static com.bakaeva.tm.constant.UserTestData.ADMIN1;
import static com.bakaeva.tm.constant.UserTestData.USER1;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private final ISessionRepository repository = new SessionRepository();

    private SessionDTO adminSession;

    private SessionDTO userSession;

    List<SessionDTO> sessionList;

    @Before
    public void setUp() {
        adminSession = new SessionDTO();
        adminSession.setUserId(ADMIN1.getId());
        adminSession.setSignature(ADMIN1.getPasswordHash());
        adminSession.setTimestamp(1L);
        userSession = new SessionDTO();
        userSession.setUserId(USER1.getId());
        userSession.setSignature(USER1.getPasswordHash());
        userSession.setTimestamp(2L);
        sessionList = Arrays.asList(adminSession, userSession);
    }

    @After
    public void tearDown() {
        repository.clear();
    }

    @Test
    public void add() {
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(adminSession);
        Assert.assertEquals(adminSession, repository.findAll().get(0));
    }

    @Test
    public void addAll() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(sessionList);
        Assert.assertEquals(sessionList, repository.findAll());
    }

    @Test
    public void remove() {
        repository.addAll(sessionList);
        Assert.assertTrue(repository.findAll().contains(userSession));
        @Nullable final SessionDTO removed = repository.remove(userSession);
        Assert.assertEquals(userSession, removed);
        Assert.assertFalse(repository.findAll().contains(userSession));
    }

    @Test
    public void load() {
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(adminSession);
        repository.load(sessionList);
        Assert.assertEquals(sessionList, repository.findAll());
    }

    @Test
    public void clear() {
        repository.addAll(sessionList);
        repository.clear();
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void findAll() {
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(adminSession);
        repository.add(userSession);
        Assert.assertEquals(
                sessionList,
                repository.findAll()
        );
    }

    @Test
    public void findById() {
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.addAll(sessionList);
        Assert.assertEquals(
                repository.findById(userSession.getId()),
                userSession
        );
    }

    @Test
    public void findByUserId() {
        repository.addAll(sessionList);
        Assert.assertTrue(repository.findByUserId(ADMIN1.getId()).contains(adminSession));
    }

    @Test
    public void removeByUserId() {
        repository.addAll(sessionList);
        repository.removeByUserId(ADMIN1.getId());
        Assert.assertFalse(repository.findByUserId(ADMIN1.getId()).contains(adminSession));
    }


}