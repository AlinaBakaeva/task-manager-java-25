package com.bakaeva.tm.service;

import com.bakaeva.tm.api.repository.ITaskRepository;
import com.bakaeva.tm.api.service.ITaskService;
import com.bakaeva.tm.dto.TaskDTO;
import com.bakaeva.tm.exception.empty.EmptyDescriptionException;
import com.bakaeva.tm.exception.empty.EmptyIdException;
import com.bakaeva.tm.exception.empty.EmptyNameException;
import com.bakaeva.tm.exception.empty.EmptyUserIdException;
import com.bakaeva.tm.exception.incorrect.IncorrectDataFileException;
import com.bakaeva.tm.exception.incorrect.IncorrectIndexException;
import com.bakaeva.tm.repository.TaskRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static com.bakaeva.tm.constant.TaskTestData.*;
import static com.bakaeva.tm.constant.UserTestData.ADMIN1;
import static com.bakaeva.tm.constant.UserTestData.USER1;
import static org.assertj.core.api.Assertions.assertThat;

public final class TaskServiceTest {

    @NotNull
    private final ITaskRepository repository = new TaskRepository();

    @NotNull
    private final ITaskService service = new TaskService(repository);

    @Rule
    @NotNull
    public final ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        repository.addAll(TASK_LIST);
    }

    @After
    public void tearDown() {
        repository.clear();
    }

    @Test
    public void findAll() {
        Assert.assertEquals(TASK_LIST, service.findAll());
    }

    @Test
    public void FindAllByUserId() {
        Assert.assertEquals(ADMIN1_TASK_LIST, service.findAll(ADMIN1.getId()));
        thrown.expect(EmptyUserIdException.class);
        service.findAll(null);
    }

    @Test
    public void clear() {
        service.clear();
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void clearByUserId() {
        service.clear(USER1.getId());
        Assert.assertTrue(repository.findAll(USER1.getId()).isEmpty());
        thrown.expect(EmptyUserIdException.class);
        service.clear(null);
    }

    @Test
    public void load() {
        service.load(USER1_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST, repository.findAll());
        thrown.expect(IncorrectDataFileException.class);
        service.load(null);
    }

    @Test
    public void remove() {
        @NotNull final List<TaskDTO> list = new ArrayList<>(repository.findAll());
        final @Nullable TaskDTO removed = service.remove(USER1_TASK2);
        Assert.assertEquals(USER1_TASK2, removed);
        list.remove(USER1_TASK2);
        Assert.assertEquals(list, repository.findAll());
        Assert.assertNull(service.remove(null));
        Assert.assertNull(service.remove(USER1_TASK2));
    }

    @Test
    public void removeByUserId() {
        @NotNull final List<TaskDTO> list = new ArrayList<>(repository.findAll());
        service.remove(USER1.getId(), USER1_TASK2);
        list.remove(USER1_TASK2);
        Assert.assertEquals(list, repository.findAll());
        service.remove(USER1.getId(), null);
        Assert.assertEquals(list, repository.findAll());
        service.remove(USER1.getId(), USER2_TASK1);
        Assert.assertEquals(list, repository.findAll());
        thrown.expect(EmptyUserIdException.class);
        service.remove(null, USER2_TASK1);
        Assert.assertEquals(list, repository.findAll());
    }

    @Test
    public void createByNameAndUserId() {
        repository.clear();
        @NotNull final TaskDTO expected = new TaskDTO();
        expected.setName(USER1_TASK1.getName());
        expected.setUserId(USER1.getId());
        service.create(USER1.getId(), USER1_TASK1.getName());
        @NotNull final TaskDTO created = repository.findAll().get(0);
        assertThat(created).isEqualToIgnoringGivenFields(expected, "id");
        thrown.expect(EmptyUserIdException.class);
        service.create(null, USER1_TASK1.getName());
        thrown.expect(EmptyNameException.class);
        service.create(USER1.getId(), null);
    }

    @Test
    public void createByNameAndUserIdAndDescription() {
        repository.clear();
        @NotNull final TaskDTO expected = new TaskDTO();
        expected.setName(USER1_TASK1.getName());
        expected.setUserId(USER1.getId());
        expected.setDescription(USER1_TASK1.getDescription());
        service.create(USER1.getId(), USER1_TASK1.getName(), USER1_TASK1.getDescription());
        @NotNull final TaskDTO created = repository.findAll().get(0);
        assertThat(created).isEqualToIgnoringGivenFields(expected, "id");
        thrown.expect(EmptyUserIdException.class);
        service.create(null, USER1_TASK1.getName(), USER1_TASK1.getDescription());
        thrown.expect(EmptyNameException.class);
        service.create(USER1.getId(), null, USER1_TASK1.getDescription());
        thrown.expect(EmptyDescriptionException.class);
        service.create(USER1.getId(), USER1_TASK1.getName(), null);
    }

    @Test
    public void addByUserId() {
        repository.clear();
        service.add(USER1.getId(), USER1_TASK1);
        Assert.assertEquals(USER1_TASK1, repository.findAll().get(0));
        service.add(USER1.getId(), null);
        Assert.assertEquals(USER1_TASK1, repository.findAll().get(0));
        thrown.expect(EmptyUserIdException.class);
        service.add(null, USER1_TASK1);
    }

    @Test
    public void findById() {
        Assert.assertEquals(
                service.findById(USER1.getId(), USER1_TASK2.getId()),
                USER1_TASK2
        );
        Assert.assertNull(repository.findById(USER1.getId(), ADMIN1_TASK2.getId()));
        thrown.expect(EmptyUserIdException.class);
        service.findById(null, USER1_TASK1.getId());
        thrown.expect(EmptyIdException.class);
        service.findById(USER1.getId(), null);
    }

    @Test
    public void findByIndex() {
        final int index = USER1_TASK_LIST.indexOf(USER1_TASK2);
        Assert.assertEquals(
                service.findByIndex(USER1.getId(), index),
                USER1_TASK2
        );
        thrown.expect(EmptyUserIdException.class);
        service.findByIndex(null, index);
        thrown.expect(EmptyUserIdException.class);
        service.findByIndex(USER1.getId(), null);
    }

    @Test
    public void findByName() {
        Assert.assertEquals(
                service.findByName(USER1.getId(), USER1_TASK2.getName()),
                USER1_TASK2
        );
        thrown.expect(EmptyUserIdException.class);
        service.findByName(null, USER1_TASK1.getId());
        thrown.expect(EmptyNameException.class);
        service.findByName(USER1.getId(), null);
    }

    @Test
    public void removeById() {
        @NotNull final List<TaskDTO> expected = new ArrayList<>(repository.findAll());
        Assert.assertEquals(
                service.removeById(USER1.getId(), USER1_TASK2.getId()),
                USER1_TASK2
        );
        expected.remove(USER1_TASK2);
        Assert.assertEquals(expected, repository.findAll());
        thrown.expect(EmptyUserIdException.class);
        service.removeById(null, USER1_TASK1.getId());
        thrown.expect(EmptyIdException.class);
        service.removeById(USER1.getId(), null);
    }

    @Test
    public void removeByIndex() {
        @NotNull final List<TaskDTO> expected = new ArrayList<>(repository.findAll());
        final int index = USER1_TASK_LIST.indexOf(USER1_TASK2);
        Assert.assertEquals(
                service.removeByIndex(USER1.getId(), index),
                USER1_TASK2
        );
        expected.remove(USER1_TASK2);
        Assert.assertEquals(expected, repository.findAll());
        thrown.expect(EmptyUserIdException.class);
        service.removeByIndex(null, index);
        thrown.expect(EmptyUserIdException.class);
        service.removeByIndex(USER1.getId(), null);
    }

    @Test
    public void removeByName() {
        @NotNull final List<TaskDTO> expected = new ArrayList<>(repository.findAll());
        Assert.assertEquals(
                service.removeByName(USER1.getId(), USER1_TASK2.getName()),
                USER1_TASK2
        );
        expected.remove(USER1_TASK2);
        Assert.assertEquals(expected, repository.findAll());
        thrown.expect(EmptyUserIdException.class);
        service.removeByName(null, USER1_TASK1.getName());
        thrown.expect(EmptyNameException.class);
        service.removeByName(USER1.getId(), null);
    }

    @Test
    public void updateById() {
        @NotNull final TaskDTO actual = new TaskDTO();
        actual.setUserId(ADMIN1.getId());
        repository.add(actual);
        service.updateById(
                ADMIN1.getId(),
                actual.getId(),
                ADMIN1_TASK1.getName(),
                ADMIN1_TASK1.getDescription()
        );
        assertThat(actual).isEqualToIgnoringGivenFields(ADMIN1_TASK1, "id");
        thrown.expect(EmptyUserIdException.class);
        service.updateById(null,
                actual.getId(),
                ADMIN1_TASK1.getName(),
                ADMIN1_TASK1.getDescription()
        );
        thrown.expect(EmptyIdException.class);
        service.updateById(ADMIN1.getId(),
                null,
                ADMIN1_TASK1.getName(),
                ADMIN1_TASK1.getDescription()
        );
        thrown.expect(EmptyNameException.class);
        service.updateById(ADMIN1.getId(),
                actual.getId(),
                null,
                ADMIN1_TASK1.getDescription()
        );
    }

    @Test
    public void updateByIndex() {
        @NotNull final TaskDTO actual = new TaskDTO();
        @NotNull final String admin1Id = ADMIN1.getId();
        actual.setUserId(admin1Id);
        repository.add(actual);
        final int index = repository.findAll(admin1Id).indexOf(actual);
        service.updateByIndex(
                admin1Id,
                index,
                ADMIN1_TASK1.getName(),
                ADMIN1_TASK1.getDescription()
        );
        assertThat(actual).isEqualToIgnoringGivenFields(ADMIN1_TASK1, "id");
        thrown.expect(EmptyUserIdException.class);
        service.updateByIndex(null,
                index,
                ADMIN1_TASK1.getName(),
                ADMIN1_TASK1.getDescription()
        );
        thrown.expect(IncorrectIndexException.class);
        service.updateByIndex(admin1Id,
                null,
                ADMIN1_TASK1.getName(),
                ADMIN1_TASK1.getDescription()
        );
        thrown.expect(EmptyNameException.class);
        service.updateByIndex(admin1Id,
                index,
                null,
                ADMIN1_TASK1.getDescription()
        );
    }

}
