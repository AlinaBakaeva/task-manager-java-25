package com.bakaeva.tm.service;

import com.bakaeva.tm.api.repository.ISessionRepository;
import com.bakaeva.tm.api.repository.IUserRepository;
import com.bakaeva.tm.api.service.IPropertyService;
import com.bakaeva.tm.api.service.ISessionService;
import com.bakaeva.tm.api.service.IUserService;
import com.bakaeva.tm.dto.SessionDTO;
import com.bakaeva.tm.enumerated.Role;
import com.bakaeva.tm.repository.SessionRepository;
import com.bakaeva.tm.repository.UserRepository;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.bakaeva.tm.constant.UserTestData.*;

public final class SessionServiceTest {

    @NotNull
    private final ISessionRepository repository = new SessionRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ISessionService service = new SessionService(repository, userService, propertyService);

    private SessionDTO adminSession;

    private SessionDTO userSession;

    @Before
    public void setUp() {
        propertyService.init();
        userRepository.addAll(USER_LIST);
        adminSession = service.open(ADMIN1.getLogin(), ADMIN1.getLogin());
        userSession = service.open(USER1.getLogin(), USER1.getLogin());
    }

    @After
    public void tearDown() {
        repository.clear();
    }

    @Test
    public void findAll() {
        Assert.assertEquals(repository.findAll(), service.findAll());
    }

    @Test
    public void findAllByUser() {
        Assert.assertEquals(repository.findByUserId(ADMIN1.getId()), service.findAll(adminSession));
    }

    @Test
    public void remove() {
        Assert.assertEquals(service.remove(userSession), userSession);
        Assert.assertTrue(repository.findByUserId(USER1.getId()).isEmpty());
    }

    @Test
    public void close() {
        service.close(userSession);
        Assert.assertTrue(repository.findByUserId(USER1.getId()).isEmpty());
    }

    @Test
    public void closeAll() {
        service.closeAll(userSession);
        Assert.assertTrue(repository.findByUserId(USER1.getId()).isEmpty());
    }

    @Test
    public void getUser() {
        Assert.assertEquals(service.getUser(adminSession), ADMIN1);
    }

    @Test
    public void getUserId() {
        Assert.assertEquals(service.getUserId(adminSession), ADMIN1.getId());
    }

    @Test
    public void sign() {
        Assert.assertNull(service.sign(null));
        Assert.assertEquals(adminSession, service.sign(adminSession));
    }

    @Test
    public void isValid() {
        Assert.assertTrue(service.isValid(adminSession));
    }

    @Test(expected = Exception.class)
    public void validateNegative() {
        service.validate(new SessionDTO());
    }

    @Test
    public void validate() {
        service.validate(userSession);
    }

    @Test(expected = Exception.class)
    public void validateWithRole() {
        service.validate(userSession, Role.ADMIN);
    }

    @Test
    public void ValidateWithRole() {
        service.validate(adminSession, Role.ADMIN);
    }

    @Test
    public void openWithWrongPassword() {
        Assert.assertNull(service.open(ADMIN1.getLogin(), "wrongPassword"));
    }

    @Test
    public void checkDataAccess() {
        Assert.assertTrue(service.checkDataAccess(ADMIN1.getLogin(), ADMIN1.getLogin()));
        Assert.assertFalse(service.checkDataAccess(ADMIN1.getLogin(), ""));
    }

    @Test
    public void signOutByLogin() {
        service.signOutByLogin(USER1.getLogin());
        Assert.assertTrue(repository.findByUserId(USER1.getLogin()).isEmpty());
    }

    @Test
    public void signOutByUserId() {
        service.signOutByUserId(USER1.getId());
        Assert.assertTrue(repository.findByUserId(USER1.getLogin()).isEmpty());
    }

}