package com.bakaeva.tm.service;

import com.bakaeva.tm.api.repository.IUserRepository;
import com.bakaeva.tm.api.service.IUserService;
import com.bakaeva.tm.dto.UserDTO;
import com.bakaeva.tm.enumerated.Role;
import com.bakaeva.tm.exception.empty.*;
import com.bakaeva.tm.exception.incorrect.IncorrectIdException;
import com.bakaeva.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserService extends AbstractService<UserDTO> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Nullable
    @Override
    public UserDTO findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }


    @Override
    public UserDTO removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeById(id);
    }

    @NotNull
    @Override
    public UserDTO create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new EmptyPasswordException();
        user.setPasswordHash(passwordHash);
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final UserDTO user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final UserDTO user = create(login, password);
        user.setRole(role);
        return user;
    }

    @NotNull
    @Override
    public UserDTO updateById(
            @Nullable final String id, @Nullable final String login,
            @Nullable final String firstName, @Nullable final String lastName,
            @Nullable final String middleName,
            @Nullable final String email
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final UserDTO user = findById(id);
        if (user == null) throw new IncorrectIdException();
        user.setLogin(login);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public UserDTO updatePasswordById(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final UserDTO user = findById(id);
        if (user == null) throw new IncorrectIdException();
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new EmptyPasswordException();
        user.setPasswordHash(passwordHash);
        return user;
    }

    @Nullable
    @Override
    public UserDTO lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) return null;
        user.setLocked(true);
        return user;
    }

    @Nullable
    @Override
    public UserDTO unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) return null;
        user.setLocked(false);
        return user;
    }

    @Nullable
    @Override
    public UserDTO removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }




    @Override
    public @Nullable UserDTO remove(@Nullable UserDTO userDTO) {
        return null;
    }
}