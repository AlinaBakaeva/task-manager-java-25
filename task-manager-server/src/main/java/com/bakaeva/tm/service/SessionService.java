package com.bakaeva.tm.service;

import com.bakaeva.tm.api.repository.ISessionRepository;
import com.bakaeva.tm.api.service.IPropertyService;
import com.bakaeva.tm.api.service.ISessionService;
import com.bakaeva.tm.api.service.IUserService;
import com.bakaeva.tm.dto.SessionDTO;
import com.bakaeva.tm.dto.UserDTO;
import com.bakaeva.tm.enumerated.Role;
import com.bakaeva.tm.exception.empty.EmptyUserIdException;
import com.bakaeva.tm.exception.security.AccessDeniedException;
import com.bakaeva.tm.util.HashUtil;
import com.bakaeva.tm.util.SignatureUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class SessionService extends AbstractService<SessionDTO> implements ISessionService {

    @NotNull
    private final ISessionRepository sessionRepository;

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    public SessionService(
            @NotNull final ISessionRepository sessionRepository,
            @NotNull final IUserService userService,
            @NotNull final IPropertyService propertyService
    ) {
        super(sessionRepository);
        this.sessionRepository = sessionRepository;
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    public void close(@NotNull final SessionDTO session) throws AccessDeniedException {
        validate(session);
        remove(session);
    }

    @Override
    public void closeAll(@NotNull final SessionDTO session) throws AccessDeniedException {
        validate(session);
        @Nullable final String userId = session.getUserId();
        if (userId == null) throw new EmptyUserIdException();
        sessionRepository.removeByUserId(userId);
    }

    @Nullable
    @Override
    public UserDTO getUser(@NotNull final SessionDTO session) throws AccessDeniedException {
        final String userId = getUserId(session);
        return userService.findById(userId);
    }

    @NotNull
    @Override
    public String getUserId(@NotNull final SessionDTO session) throws AccessDeniedException {
        validate(session);
        @Nullable final String userId = session.getUserId();
        if (userId == null) throw new EmptyUserIdException();
        return session.getUserId();
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll(@NotNull final SessionDTO session) throws AccessDeniedException {
        validate(session);
        @Nullable final String userId = session.getUserId();
        if (userId == null) throw new EmptyUserIdException();
        return sessionRepository.findByUserId(session.getUserId());
    }

    @Nullable
    @Override
    public SessionDTO sign(@Nullable final SessionDTO session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @Override
    public boolean isValid(@NotNull final SessionDTO session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void validate(@Nullable final SessionDTO session) throws AccessDeniedException {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null) throw new AccessDeniedException();
        if (session.getUserId() == null) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final SessionDTO temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final SessionDTO tempSigned = sign(temp);
        if (tempSigned == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = tempSigned.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (sessionRepository.findById(session.getId()) == null) throw new AccessDeniedException();
    }

    @Override
    public void validate(@NotNull final SessionDTO session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final UserDTO user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (user.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    public SessionDTO open(@NotNull final String login, @NotNull final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        final UserDTO user = userService.findByLogin(login);
        if (user == null) return null;
        final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.add(session);
        return sign(session);
    }

    @Override
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        final UserDTO user = userService.findByLogin(login);
        if (user == null) return false;
        final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    public void signOutByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return;
        @Nullable final UserDTO user = userService.findByLogin(login);
        if (user == null) return;
        @NotNull final String userId = user.getId();
        sessionRepository.removeByUserId(userId);
    }

    @Override
    public void signOutByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        sessionRepository.removeByUserId(userId);
    }


}