package com.bakaeva.tm.service;

import com.bakaeva.tm.api.IRepository;
import com.bakaeva.tm.api.IService;
import com.bakaeva.tm.dto.AbstractEntityDTO;
import com.bakaeva.tm.exception.incorrect.IncorrectDataFileException;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@AllArgsConstructor
public abstract class AbstractService<E extends AbstractEntityDTO> implements IService<E> {

    @NotNull
    protected final IRepository<E> repository;

    @Override
    public @NotNull List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public void load(@Nullable final List<E> list) {
        if (list == null) throw new IncorrectDataFileException();
        repository.load(list);
    }

    @Override
    public @Nullable E remove(@Nullable final E e) {
        if (e == null) return null;
        return repository.remove(e);
    }

}