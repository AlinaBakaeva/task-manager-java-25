package com.bakaeva.tm.service;

import com.bakaeva.tm.api.service.IPropertyService;
import com.bakaeva.tm.constant.PropertyConstant;
import com.bakaeva.tm.exception.incorrect.IncorrectPropertyFileException;
import com.bakaeva.tm.exception.system.NoSuchPropertyException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull
    private final Properties properties = new Properties();

    @Override
    public void init() {
        @NotNull final String path = PropertyConstant.FILE_APPLICATION_PROPERTIES;
        try (@Nullable final InputStream inputStream = PropertyService.class.getResourceAsStream(path)) {
            properties.load(inputStream);
        } catch (IOException e) {
            throw new IncorrectPropertyFileException(path.substring(1));
        }
    }

    @Override
    @NotNull
    public String getServerHost() {
        @Nullable final String propertyHost = properties.getProperty(PropertyConstant.SERVER_HOST);
        @Nullable final String envHost = System.getProperty(PropertyConstant.SERVER_HOST);
        if (envHost != null) return envHost;
        if (propertyHost == null) throw new NoSuchPropertyException(PropertyConstant.SERVER_HOST);
        return propertyHost;
    }

    @Override
    @NotNull
    public Integer getServerPort() {
        @Nullable final String propertyPort = properties.getProperty(PropertyConstant.SERVER_PORT);
        @Nullable final String envPort = System.getProperty(PropertyConstant.SERVER_PORT);
        String value = propertyPort;
        if (envPort != null) value = envPort;
        if (value == null) throw new NoSuchPropertyException(PropertyConstant.SERVER_PORT);
        return Integer.parseInt(value);
    }

    @Override
    @NotNull
    public String getSessionSalt() {
        @Nullable final String sessionSalt = properties.getProperty(PropertyConstant.SESSION_SALT);
        if (sessionSalt == null) throw new NoSuchPropertyException(PropertyConstant.SESSION_SALT);
        return sessionSalt;
    }

    @Override
    @NotNull
    public Integer getSessionCycle() {
        @Nullable final String sessionCycle = properties.getProperty(PropertyConstant.SESSION_CYCLE);
        if (sessionCycle == null) throw new NoSuchPropertyException(PropertyConstant.SESSION_CYCLE);
        return Integer.parseInt(sessionCycle);
    }

    @Override
    public @NotNull String getJdbcDriver() {
        @Nullable final String jdbcDriver = properties.getProperty(PropertyConstant.JDBC_DRIVER);
        if (jdbcDriver == null) throw new NoSuchPropertyException(PropertyConstant.JDBC_DRIVER);
        return jdbcDriver;
    }

    @Override
    public @NotNull String getJdbcPassword() {
        @Nullable final String jdbcPassword = properties.getProperty(PropertyConstant.JDBC_PASSWORD);
        if (jdbcPassword == null) throw new NoSuchPropertyException(PropertyConstant.JDBC_PASSWORD);
        return jdbcPassword;
    }

    @Override
    public @NotNull String getJdbcLogin() {
        @Nullable final String jdbcLogin = properties.getProperty(PropertyConstant.JDBC_LOGIN);
        if (jdbcLogin == null) throw new NoSuchPropertyException(PropertyConstant.JDBC_LOGIN);
        return jdbcLogin;
    }

    @Override
    public @NotNull String getJdbcUrl() {
        @Nullable final String jdbcUrl = properties.getProperty(PropertyConstant.JDBC_URL);
        if (jdbcUrl == null) throw new NoSuchPropertyException(PropertyConstant.JDBC_URL);
        return jdbcUrl;
    }

}