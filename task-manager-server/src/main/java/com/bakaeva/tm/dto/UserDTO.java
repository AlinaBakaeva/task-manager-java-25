package com.bakaeva.tm.dto;

import com.bakaeva.tm.entity.User;
import com.bakaeva.tm.enumerated.Role;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class UserDTO extends AbstractEntityDTO {

    public static final long serialVersionUID = 1L;

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @Nullable
    private Role role = Role.USER;

    @NotNull
    private Boolean locked = false;

    public UserDTO(@NotNull final User user) {
        this.login = user.getLogin();
        this.passwordHash = user.getPasswordHash();
        this.email = user.getEmail();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.middleName = user.getMiddleName();
        this.role = user.getRole();
        this.locked = user.getLocked();
        this.setId(user.getId());
    }

    @Nullable
    public static UserDTO toDTO(@Nullable final User user) {
        if (user == null) return null;
        return new UserDTO(user);
    }

}