package com.bakaeva.tm.dto;

import com.bakaeva.tm.entity.Session;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;

@Getter
@Setter
@Entity
@NoArgsConstructor
public final class SessionDTO extends AbstractEntityDTO implements Cloneable {

    @Nullable
    private Long timestamp;

    @Nullable
    private String userId;

    @Nullable
    private String signature;

    public SessionDTO(@NotNull final Session session) {
        this.timestamp = session.getTimestamp();
        this.userId = session.getUser().getId();
        this.signature = session.getSignature();
        this.setId(session.getId());
    }
    
    @Nullable
    @Override
    public SessionDTO clone() {
        try {
            return (SessionDTO) super.clone();
        } catch (@NotNull final CloneNotSupportedException e) {
            return null;
        }
    }

    @Nullable
    public static SessionDTO toDTO(@Nullable final Session session) {
        if (session == null) return null;
        return new SessionDTO(session);
    }

}