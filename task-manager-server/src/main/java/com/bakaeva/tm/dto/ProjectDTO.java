package com.bakaeva.tm.dto;

import com.bakaeva.tm.entity.Project;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class ProjectDTO extends AbstractEntityDTO {

    public static final long serialVersionUID = 1L;

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    private String userId;

    public ProjectDTO(@NotNull final Project project) {
        this.name = project.getName();
        this.description = project.getDescription();
        this.userId = project.getUser().getId();
        this.setId(project.getId());
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

    @Nullable
    public static ProjectDTO toDTO(@Nullable final Project project) {
        if (project == null) return null;
        return new ProjectDTO(project);
    }
    
}