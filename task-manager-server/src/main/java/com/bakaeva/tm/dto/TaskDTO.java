package com.bakaeva.tm.dto;

import com.bakaeva.tm.entity.Task;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class TaskDTO extends AbstractEntityDTO {

    public static final long serialVersionUID = 1L;

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    private String userId;

    public TaskDTO(@NotNull final Task task) {
        this.name = task.getName();
        this.description = task.getDescription();
        this.userId = task.getUser().getId();
        this.setId(task.getId());
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

    @Nullable
    public static TaskDTO toDTO(@Nullable final Task task) {
        if (task == null) return null;
        return new TaskDTO(task);
    }

}
