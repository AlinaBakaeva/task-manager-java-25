package com.bakaeva.tm.constant;

public interface DataConstant {

    String FILE_BINARY = "./data.bin";

    String FILE_BASE64 = "./data.base64";

    String FILE_XML = "./data.xml";

    String FILE_JSON = "./data.json";

}