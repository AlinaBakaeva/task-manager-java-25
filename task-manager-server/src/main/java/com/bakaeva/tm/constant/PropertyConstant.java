package com.bakaeva.tm.constant;

import org.jetbrains.annotations.NotNull;

public interface PropertyConstant {

    @NotNull
    String FILE_APPLICATION_PROPERTIES = "/application.properties";

    @NotNull
    String SERVER_HOST = "server.host";

    @NotNull
    String SERVER_PORT = "server.port";

    @NotNull
    String SESSION_SALT = "session.salt";

    @NotNull
    String SESSION_CYCLE = "session.cycle";

    @NotNull
    String JDBC_LOGIN = "jdbc.login";

    @NotNull
    String JDBC_PASSWORD = "jdbc.password";

    @NotNull
    String JDBC_URL = "jdbc.url";

    @NotNull
    String JDBC_DRIVER = "jdbc.driver";

}