package com.bakaeva.tm.repository;

import com.bakaeva.tm.api.repository.IProjectRepository;
import com.bakaeva.tm.dto.ProjectDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<ProjectDTO> implements IProjectRepository {

    @Override
    public void add(@NotNull final String userId, @NotNull final ProjectDTO project) {
        project.setUserId(userId);
        entities.add(project);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final ProjectDTO project) {
        if (!userId.equals(project.getUserId())) return;
        entities.remove(project);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<ProjectDTO> userProjects = findAll(userId);
        entities.removeAll(userProjects);
    }

        @Override
    public List<ProjectDTO> findAll(@NotNull final String userId) {
        @NotNull final List<ProjectDTO> result = new ArrayList<>();
        for (@Nullable final ProjectDTO project : entities) {
            if (project == null) continue;
            if (userId.equals(project.getUserId())) result.add((project));
        }
        return result;
    }

    @Nullable
    @Override
    public ProjectDTO findById(@NotNull final String userId, @NotNull final String id) {
        for (@Nullable final ProjectDTO project : entities) {
            if (project == null) continue;
            if (id.equals((project.getId())) && userId.equals(project.getUserId()))
                return project;
        }
        return null;
    }

    @Nullable
    @Override
    public ProjectDTO findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return findAll(userId).get(index);
    }

    @Nullable
    @Override
    public ProjectDTO findByName(@NotNull final String userId, @NotNull final String name) {
        for (@Nullable final ProjectDTO project : entities) {
            if (project == null) continue;
            if (name.equals((project.getName())) && userId.equals(project.getUserId()))
                return project;
        }
        return null;
    }

    @Nullable
    @Override
    public ProjectDTO removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final ProjectDTO project = findById(userId, id);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Override
    public ProjectDTO removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final ProjectDTO project = findByIndex(userId, index);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Override
    @Nullable
    public ProjectDTO removeByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final ProjectDTO project = findByName(userId, name);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

}