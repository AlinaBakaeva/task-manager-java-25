package com.bakaeva.tm.repository;

import com.bakaeva.tm.api.repository.IUserRepository;
import com.bakaeva.tm.dto.UserDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserRepository extends AbstractRepository<UserDTO> implements IUserRepository {

    @Nullable
    @Override
    public UserDTO findById(@NotNull final String id) {
        for (@Nullable final UserDTO user : entities) {
            if (user == null) continue;
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@NotNull final String login) {
        for (@Nullable final UserDTO user : entities) {
            if (user == null) continue;
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public UserDTO removeById(@NotNull final String id) {
        @Nullable final UserDTO user = findById(id);
        if (user == null) return null;
        return remove(user);
    }

    @Nullable
    @Override
    public UserDTO removeByLogin(@NotNull final String login) {
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) return null;
        return remove(user);
    }

}