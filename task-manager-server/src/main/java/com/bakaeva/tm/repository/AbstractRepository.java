package com.bakaeva.tm.repository;

import com.bakaeva.tm.api.IRepository;
import com.bakaeva.tm.dto.AbstractEntityDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository <E extends AbstractEntityDTO> implements IRepository<E> {

    @NotNull
    protected final List<E> entities = new ArrayList<>();

    @Override
    public void add(@NotNull final E e) {
        entities.add(e);
    }

    @Override
    public void addAll(final List<E> list) {
        entities.addAll(list);
    }

    @Nullable
    @Override
    public E remove(@NotNull E e) {
        if (!entities.contains(e)) return null;
        entities.remove(e);
        return e;
    }

    @Override
    public void load(@NotNull final List<E> list) {
        clear();
        addAll(list);
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return entities;
    }

}