package com.bakaeva.tm.repository;

import com.bakaeva.tm.api.repository.ITaskRepository;
import com.bakaeva.tm.dto.TaskDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<TaskDTO> implements ITaskRepository{

    @Override
    public void add(@NotNull final String userId, @NotNull final TaskDTO task) {
        entities.add(task);
        task.setUserId(userId);
    }

    @Override
    public void remove(@NotNull final String userId, final TaskDTO task) {
        if (!userId.equals(task.getUserId())) return;
        entities.remove(task);
    }

    @Override
    public List<TaskDTO> findAll(@NotNull final String userId) {
        @NotNull final List<TaskDTO> result = new ArrayList<>();
        for (@NotNull final TaskDTO task : entities) {
            if (task == null) continue;
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<TaskDTO> userTasks = findAll(userId);
        entities.removeAll(userTasks);
    }

    @Nullable
    @Override
    public TaskDTO findById(@NotNull final String userId, @NotNull final String id) {
        for (@Nullable final TaskDTO task : entities) {
            if (task == null) continue;
            if (id.equals((task.getId())) && userId.equals(task.getUserId()))
                return task;
        }
        return null;
    }

    @Nullable
    @Override
    public TaskDTO findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return findAll(userId).get(index);
    }

    @Nullable
    @Override
    public TaskDTO findByName(@NotNull final String userId, @NotNull final String name) {
        for (@Nullable final TaskDTO task : entities) {
            if (task == null) continue;
            if (name.equals((task.getName())) && userId.equals(task.getUserId()))
                return task;
        }
        return null;
    }

    @Nullable
    @Override
    public TaskDTO removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable TaskDTO task = findById(userId, id);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public TaskDTO removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final TaskDTO task = findByIndex(userId, index);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Nullable
    @Override
    public TaskDTO removeByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final TaskDTO task = findByName(userId, name);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

}