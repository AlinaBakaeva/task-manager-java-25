package com.bakaeva.tm.api.service;

import com.bakaeva.tm.api.IService;
import com.bakaeva.tm.dto.ProjectDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectService  extends IService<ProjectDTO> {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void add(@Nullable String userId, @Nullable ProjectDTO project);

    @Nullable ProjectDTO remove(@Nullable String userId, @Nullable ProjectDTO project);

    void clear(@Nullable String userId);

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId);

    @Nullable
    ProjectDTO findById(@Nullable String userId, @Nullable String id);

    @Nullable
    ProjectDTO findByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    ProjectDTO findByName(@Nullable String userId, @Nullable String name);

    @Nullable
    ProjectDTO removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    ProjectDTO removeByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    ProjectDTO removeByName(@Nullable String userId, @Nullable String name);

    @NotNull
    ProjectDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    ProjectDTO updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

}