package com.bakaeva.tm.api.endpoint;

import com.bakaeva.tm.dto.SessionDTO;
import com.bakaeva.tm.dto.UserDTO;
import com.bakaeva.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IUserEndpoint {

    @Nullable
    @WebMethod
    UserDTO createUser(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    );

    @NotNull
    @WebMethod
    UserDTO createUserWithEmail(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password,
            @WebParam(name = "email") @Nullable String email
    );

    @NotNull
    @WebMethod
    UserDTO registerUserWithEmail(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password,
            @WebParam(name = "email") @Nullable String email
    );

    @NotNull
    @WebMethod
    UserDTO createUserWithRole(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password,
            @WebParam(name = "role") @Nullable Role role
    );

    @Nullable
    @WebMethod
    UserDTO findUserById(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "id") @Nullable String id
    );

    @Nullable
    @WebMethod
    UserDTO findUserByLogin(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "login") @Nullable String login
    );

    @Nullable
    @WebMethod
    UserDTO removeUserById(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "id") @Nullable String id
    );

    @Nullable
    @WebMethod
    UserDTO removeUserByLogin(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "login") @Nullable String login
    );

    @NotNull
    @WebMethod
    UserDTO updateUser(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "firstName") @Nullable String firstName,
            @WebParam(name = "lastName") @Nullable String lastName,
            @WebParam(name = "middleName") @Nullable String middleName,
            @WebParam(name = "email") @Nullable String email
    );

    @NotNull
    @WebMethod
    UserDTO updateUserById(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "id") @Nullable String id,
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "firstName") @Nullable String firstName,
            @WebParam(name = "lastName") @Nullable String lastName,
            @WebParam(name = "middleName") @Nullable String middleName,
            @WebParam(name = "email") @Nullable String email
    );

    @NotNull
    @WebMethod
    UserDTO updateUserPassword(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "password") @Nullable String password
    );

    @NotNull
    @WebMethod
    UserDTO updateUserPasswordById(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "id") @Nullable String id,
            @WebParam(name = "password") @Nullable String password
    );

    @Nullable
    @WebMethod
    UserDTO lockUserByLogin(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "login") @Nullable String login
    );

    @Nullable
    @WebMethod
    UserDTO unlockUserByLogin(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "login") @Nullable String login
    );

    @NotNull
    @WebMethod
    List<UserDTO> findUserAll(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    void removeUserAll(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @Nullable
    @WebMethod
    UserDTO removeUser(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "user") @Nullable UserDTO user
    );

}
