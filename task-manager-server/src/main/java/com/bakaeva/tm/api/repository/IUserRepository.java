package com.bakaeva.tm.api.repository;

import com.bakaeva.tm.api.IRepository;
import com.bakaeva.tm.dto.UserDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IUserRepository extends IRepository<UserDTO> {

    @Nullable
    UserDTO findById(@NotNull String id);

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Nullable
    UserDTO removeById(@NotNull String id);

    @Nullable
    UserDTO removeByLogin(@NotNull String login);
    
}