package com.bakaeva.tm.api;

import com.bakaeva.tm.dto.AbstractEntityDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IRepository<E extends AbstractEntityDTO> {

    @NotNull
    List<E> findAll();

    void add(@NotNull E e);

    void addAll(List<E> list);

    @Nullable
    E remove(@NotNull E e);

    void load(@NotNull List<E> list);

    void clear();

}