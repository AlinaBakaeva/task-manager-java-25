package com.bakaeva.tm.api.service;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public interface IEntityManagerService {

    @NotNull EntityManager getEntityManager();

    void init();

}
