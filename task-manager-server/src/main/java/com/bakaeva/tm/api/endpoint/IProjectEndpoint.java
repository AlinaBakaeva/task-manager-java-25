package com.bakaeva.tm.api.endpoint;

import com.bakaeva.tm.dto.ProjectDTO;
import com.bakaeva.tm.dto.SessionDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @WebMethod
    void createProject(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "name") @Nullable String name
    );

    @WebMethod
    void createProjectWithDescription(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    );

    @WebMethod
    void addProject(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "project") @Nullable ProjectDTO project
    );

    @WebMethod
    void removeProject(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "project") @Nullable ProjectDTO project
    );

    @WebMethod
    void removeProjectByUserAll(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    void removeProjectAll(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @NotNull
    @WebMethod
    List<ProjectDTO> findProjectAll(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @Nullable
    @WebMethod
    ProjectDTO findProjectById(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "id") @Nullable String id
    );

    @Nullable
    @WebMethod
    ProjectDTO findProjectByIndex(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "index") @Nullable Integer index
    );

    @Nullable
    @WebMethod
    ProjectDTO findProjectByName(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "name") @Nullable String name
    );

    @Nullable
    @WebMethod
    ProjectDTO removeProjectById(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "id") @Nullable String id
    );

    @Nullable
    @WebMethod
    ProjectDTO removeProjectByIndex(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "index") @Nullable Integer index
    );

    @Nullable
    @WebMethod
    ProjectDTO removeProjectByName(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "name") @Nullable String name
    );

    @NotNull
    @WebMethod
    ProjectDTO updateProjectById(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "id") @Nullable String id,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    );

    @NotNull
    @WebMethod
    ProjectDTO updateProjectByIndex(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "index") @Nullable Integer index,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    );

}