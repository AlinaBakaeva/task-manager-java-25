package com.bakaeva.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
@NoArgsConstructor
public final class Session extends AbstractEntity implements Cloneable {

    @Nullable
    private Long timestamp;

    @NotNull
    @ManyToOne
    private User user;

    @Nullable
    private String signature;

    @Nullable
    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (@NotNull final CloneNotSupportedException e) {
            return null;
        }
    }

}