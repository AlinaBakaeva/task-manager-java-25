package com.bakaeva.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Project extends AbstractEntity {

    public static final long serialVersionUID = 1L;

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    @ManyToOne
    private User user;

    @OneToMany(mappedBy = "project", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    public List<Task> tasks;

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}