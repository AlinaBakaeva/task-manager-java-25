
package com.bakaeva.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.bakaeva.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _LoadFromBase64_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "loadFromBase64");
    private final static QName _LoadFromBase64Response_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "loadFromBase64Response");
    private final static QName _LoadFromBinary_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "loadFromBinary");
    private final static QName _LoadFromBinaryResponse_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "loadFromBinaryResponse");
    private final static QName _LoadFromJson_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "loadFromJson");
    private final static QName _LoadFromJsonResponse_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "loadFromJsonResponse");
    private final static QName _LoadFromXml_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "loadFromXml");
    private final static QName _LoadFromXmlResponse_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "loadFromXmlResponse");
    private final static QName _RemoveBase64_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "removeBase64");
    private final static QName _RemoveBase64Response_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "removeBase64Response");
    private final static QName _RemoveBinary_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "removeBinary");
    private final static QName _RemoveBinaryResponse_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "removeBinaryResponse");
    private final static QName _RemoveJson_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "removeJson");
    private final static QName _RemoveJsonResponse_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "removeJsonResponse");
    private final static QName _RemoveXml_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "removeXml");
    private final static QName _RemoveXmlResponse_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "removeXmlResponse");
    private final static QName _SaveToBase64_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "saveToBase64");
    private final static QName _SaveToBase64Response_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "saveToBase64Response");
    private final static QName _SaveToBinary_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "saveToBinary");
    private final static QName _SaveToBinaryResponse_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "saveToBinaryResponse");
    private final static QName _SaveToJson_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "saveToJson");
    private final static QName _SaveToJsonResponse_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "saveToJsonResponse");
    private final static QName _SaveToXml_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "saveToXml");
    private final static QName _SaveToXmlResponse_QNAME = new QName("http://endpoint.tm.bakaeva.com/", "saveToXmlResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.bakaeva.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LoadFromBase64 }
     * 
     */
    public LoadFromBase64 createLoadFromBase64() {
        return new LoadFromBase64();
    }

    /**
     * Create an instance of {@link LoadFromBase64Response }
     * 
     */
    public LoadFromBase64Response createLoadFromBase64Response() {
        return new LoadFromBase64Response();
    }

    /**
     * Create an instance of {@link LoadFromBinary }
     * 
     */
    public LoadFromBinary createLoadFromBinary() {
        return new LoadFromBinary();
    }

    /**
     * Create an instance of {@link LoadFromBinaryResponse }
     * 
     */
    public LoadFromBinaryResponse createLoadFromBinaryResponse() {
        return new LoadFromBinaryResponse();
    }

    /**
     * Create an instance of {@link LoadFromJson }
     * 
     */
    public LoadFromJson createLoadFromJson() {
        return new LoadFromJson();
    }

    /**
     * Create an instance of {@link LoadFromJsonResponse }
     * 
     */
    public LoadFromJsonResponse createLoadFromJsonResponse() {
        return new LoadFromJsonResponse();
    }

    /**
     * Create an instance of {@link LoadFromXml }
     * 
     */
    public LoadFromXml createLoadFromXml() {
        return new LoadFromXml();
    }

    /**
     * Create an instance of {@link LoadFromXmlResponse }
     * 
     */
    public LoadFromXmlResponse createLoadFromXmlResponse() {
        return new LoadFromXmlResponse();
    }

    /**
     * Create an instance of {@link RemoveBase64 }
     * 
     */
    public RemoveBase64 createRemoveBase64() {
        return new RemoveBase64();
    }

    /**
     * Create an instance of {@link RemoveBase64Response }
     * 
     */
    public RemoveBase64Response createRemoveBase64Response() {
        return new RemoveBase64Response();
    }

    /**
     * Create an instance of {@link RemoveBinary }
     * 
     */
    public RemoveBinary createRemoveBinary() {
        return new RemoveBinary();
    }

    /**
     * Create an instance of {@link RemoveBinaryResponse }
     * 
     */
    public RemoveBinaryResponse createRemoveBinaryResponse() {
        return new RemoveBinaryResponse();
    }

    /**
     * Create an instance of {@link RemoveJson }
     * 
     */
    public RemoveJson createRemoveJson() {
        return new RemoveJson();
    }

    /**
     * Create an instance of {@link RemoveJsonResponse }
     * 
     */
    public RemoveJsonResponse createRemoveJsonResponse() {
        return new RemoveJsonResponse();
    }

    /**
     * Create an instance of {@link RemoveXml }
     * 
     */
    public RemoveXml createRemoveXml() {
        return new RemoveXml();
    }

    /**
     * Create an instance of {@link RemoveXmlResponse }
     * 
     */
    public RemoveXmlResponse createRemoveXmlResponse() {
        return new RemoveXmlResponse();
    }

    /**
     * Create an instance of {@link SaveToBase64 }
     * 
     */
    public SaveToBase64 createSaveToBase64() {
        return new SaveToBase64();
    }

    /**
     * Create an instance of {@link SaveToBase64Response }
     * 
     */
    public SaveToBase64Response createSaveToBase64Response() {
        return new SaveToBase64Response();
    }

    /**
     * Create an instance of {@link SaveToBinary }
     * 
     */
    public SaveToBinary createSaveToBinary() {
        return new SaveToBinary();
    }

    /**
     * Create an instance of {@link SaveToBinaryResponse }
     * 
     */
    public SaveToBinaryResponse createSaveToBinaryResponse() {
        return new SaveToBinaryResponse();
    }

    /**
     * Create an instance of {@link SaveToJson }
     * 
     */
    public SaveToJson createSaveToJson() {
        return new SaveToJson();
    }

    /**
     * Create an instance of {@link SaveToJsonResponse }
     * 
     */
    public SaveToJsonResponse createSaveToJsonResponse() {
        return new SaveToJsonResponse();
    }

    /**
     * Create an instance of {@link SaveToXml }
     * 
     */
    public SaveToXml createSaveToXml() {
        return new SaveToXml();
    }

    /**
     * Create an instance of {@link SaveToXmlResponse }
     * 
     */
    public SaveToXmlResponse createSaveToXmlResponse() {
        return new SaveToXmlResponse();
    }

    /**
     * Create an instance of {@link SessionDTO }
     * 
     */
    public SessionDTO createSession() {
        return new SessionDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromBase64 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "loadFromBase64")
    public JAXBElement<LoadFromBase64> createLoadFromBase64(LoadFromBase64 value) {
        return new JAXBElement<LoadFromBase64>(_LoadFromBase64_QNAME, LoadFromBase64 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromBase64Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "loadFromBase64Response")
    public JAXBElement<LoadFromBase64Response> createLoadFromBase64Response(LoadFromBase64Response value) {
        return new JAXBElement<LoadFromBase64Response>(_LoadFromBase64Response_QNAME, LoadFromBase64Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromBinary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "loadFromBinary")
    public JAXBElement<LoadFromBinary> createLoadFromBinary(LoadFromBinary value) {
        return new JAXBElement<LoadFromBinary>(_LoadFromBinary_QNAME, LoadFromBinary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromBinaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "loadFromBinaryResponse")
    public JAXBElement<LoadFromBinaryResponse> createLoadFromBinaryResponse(LoadFromBinaryResponse value) {
        return new JAXBElement<LoadFromBinaryResponse>(_LoadFromBinaryResponse_QNAME, LoadFromBinaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "loadFromJson")
    public JAXBElement<LoadFromJson> createLoadFromJson(LoadFromJson value) {
        return new JAXBElement<LoadFromJson>(_LoadFromJson_QNAME, LoadFromJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "loadFromJsonResponse")
    public JAXBElement<LoadFromJsonResponse> createLoadFromJsonResponse(LoadFromJsonResponse value) {
        return new JAXBElement<LoadFromJsonResponse>(_LoadFromJsonResponse_QNAME, LoadFromJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "loadFromXml")
    public JAXBElement<LoadFromXml> createLoadFromXml(LoadFromXml value) {
        return new JAXBElement<LoadFromXml>(_LoadFromXml_QNAME, LoadFromXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "loadFromXmlResponse")
    public JAXBElement<LoadFromXmlResponse> createLoadFromXmlResponse(LoadFromXmlResponse value) {
        return new JAXBElement<LoadFromXmlResponse>(_LoadFromXmlResponse_QNAME, LoadFromXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveBase64 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "removeBase64")
    public JAXBElement<RemoveBase64> createRemoveBase64(RemoveBase64 value) {
        return new JAXBElement<RemoveBase64>(_RemoveBase64_QNAME, RemoveBase64 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveBase64Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "removeBase64Response")
    public JAXBElement<RemoveBase64Response> createRemoveBase64Response(RemoveBase64Response value) {
        return new JAXBElement<RemoveBase64Response>(_RemoveBase64Response_QNAME, RemoveBase64Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveBinary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "removeBinary")
    public JAXBElement<RemoveBinary> createRemoveBinary(RemoveBinary value) {
        return new JAXBElement<RemoveBinary>(_RemoveBinary_QNAME, RemoveBinary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveBinaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "removeBinaryResponse")
    public JAXBElement<RemoveBinaryResponse> createRemoveBinaryResponse(RemoveBinaryResponse value) {
        return new JAXBElement<RemoveBinaryResponse>(_RemoveBinaryResponse_QNAME, RemoveBinaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "removeJson")
    public JAXBElement<RemoveJson> createRemoveJson(RemoveJson value) {
        return new JAXBElement<RemoveJson>(_RemoveJson_QNAME, RemoveJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "removeJsonResponse")
    public JAXBElement<RemoveJsonResponse> createRemoveJsonResponse(RemoveJsonResponse value) {
        return new JAXBElement<RemoveJsonResponse>(_RemoveJsonResponse_QNAME, RemoveJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "removeXml")
    public JAXBElement<RemoveXml> createRemoveXml(RemoveXml value) {
        return new JAXBElement<RemoveXml>(_RemoveXml_QNAME, RemoveXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "removeXmlResponse")
    public JAXBElement<RemoveXmlResponse> createRemoveXmlResponse(RemoveXmlResponse value) {
        return new JAXBElement<RemoveXmlResponse>(_RemoveXmlResponse_QNAME, RemoveXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveToBase64 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "saveToBase64")
    public JAXBElement<SaveToBase64> createSaveToBase64(SaveToBase64 value) {
        return new JAXBElement<SaveToBase64>(_SaveToBase64_QNAME, SaveToBase64 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveToBase64Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "saveToBase64Response")
    public JAXBElement<SaveToBase64Response> createSaveToBase64Response(SaveToBase64Response value) {
        return new JAXBElement<SaveToBase64Response>(_SaveToBase64Response_QNAME, SaveToBase64Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveToBinary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "saveToBinary")
    public JAXBElement<SaveToBinary> createSaveToBinary(SaveToBinary value) {
        return new JAXBElement<SaveToBinary>(_SaveToBinary_QNAME, SaveToBinary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveToBinaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "saveToBinaryResponse")
    public JAXBElement<SaveToBinaryResponse> createSaveToBinaryResponse(SaveToBinaryResponse value) {
        return new JAXBElement<SaveToBinaryResponse>(_SaveToBinaryResponse_QNAME, SaveToBinaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveToJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "saveToJson")
    public JAXBElement<SaveToJson> createSaveToJson(SaveToJson value) {
        return new JAXBElement<SaveToJson>(_SaveToJson_QNAME, SaveToJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveToJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "saveToJsonResponse")
    public JAXBElement<SaveToJsonResponse> createSaveToJsonResponse(SaveToJsonResponse value) {
        return new JAXBElement<SaveToJsonResponse>(_SaveToJsonResponse_QNAME, SaveToJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveToXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "saveToXml")
    public JAXBElement<SaveToXml> createSaveToXml(SaveToXml value) {
        return new JAXBElement<SaveToXml>(_SaveToXml_QNAME, SaveToXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveToXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakaeva.com/", name = "saveToXmlResponse")
    public JAXBElement<SaveToXmlResponse> createSaveToXmlResponse(SaveToXmlResponse value) {
        return new JAXBElement<SaveToXmlResponse>(_SaveToXmlResponse_QNAME, SaveToXmlResponse.class, null, value);
    }

}
