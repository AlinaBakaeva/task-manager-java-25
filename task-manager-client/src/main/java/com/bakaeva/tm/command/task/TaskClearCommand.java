package com.bakaeva.tm.command.task;

import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.endpoint.SessionDTO;
import com.bakaeva.tm.exception.security.AccessDeniedException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-clear";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() {
        final @Nullable SessionDTO session = endpointLocator.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[CLEAR TASKS]");
        endpointLocator.getTaskEndpoint().removeTaskByUserAll(session);
        System.out.println("[OK]");
    }

}