package com.bakaeva.tm.command.system;

import com.bakaeva.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import com.bakaeva.tm.bootstrap.Bootstrap;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String argument() {
        return "-h";
    }

    @NotNull
    @Override
    public String description() {
        return "Display terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Bootstrap bootstrap = (Bootstrap) endpointLocator;
        @NotNull final Collection<AbstractCommand> commands = bootstrap.getCommands();
        for (@Nullable AbstractCommand command : commands) {
            if (command == null) continue;
            System.out.printf("%-25s| %s\n", command.name(), command.description());
        }
        System.out.println("[OK]");
    }

}