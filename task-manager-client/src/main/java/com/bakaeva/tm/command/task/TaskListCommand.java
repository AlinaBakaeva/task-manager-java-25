package com.bakaeva.tm.command.task;

import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.endpoint.SessionDTO;
import com.bakaeva.tm.endpoint.Task;
import com.bakaeva.tm.exception.security.AccessDeniedException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class TaskListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list.";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = endpointLocator.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[LIST TASKS]");
        @NotNull final List<Task> tasks = endpointLocator.getTaskEndpoint().findTaskAll(session);
        for (@Nullable final Task task : tasks) System.out.println(task);
        System.out.println("[OK]");
    }

}