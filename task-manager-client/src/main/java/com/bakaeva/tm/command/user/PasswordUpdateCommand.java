package com.bakaeva.tm.command.user;

import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.endpoint.SessionDTO;
import com.bakaeva.tm.endpoint.User;
import com.bakaeva.tm.exception.security.AccessDeniedException;
import com.bakaeva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class PasswordUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "password-update";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update my password.";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = endpointLocator.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[UPDATE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = endpointLocator.getUserEndpoint().updateUserPassword(
                session, password
        );
        if (user == null) System.out.println("[FAIL]");
        System.out.println("[OK]");
    }


}