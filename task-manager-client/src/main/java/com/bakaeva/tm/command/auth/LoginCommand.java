package com.bakaeva.tm.command.auth;

import com.bakaeva.tm.endpoint.SessionDTO;
import com.bakaeva.tm.util.TerminalUtil;
import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.endpoint.SessionEndpoint;
import com.bakaeva.tm.exception.security.AccessDeniedException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


public final class LoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Log in.";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("[ENTER LOGIN:]");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final SessionEndpoint sessionEndpoint = endpointLocator.getSessionEndpoint();
        @Nullable final SessionDTO session = sessionEndpoint.openSession(login, password);
        if (session == null) throw new AccessDeniedException();
        endpointLocator.setCurrentSession(session);
        System.out.println("[OK:]");
    }

}
