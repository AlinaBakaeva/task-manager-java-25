package com.bakaeva.tm.command.user;

import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.endpoint.SessionDTO;
import com.bakaeva.tm.endpoint.User;
import com.bakaeva.tm.exception.security.AccessDeniedException;
import com.bakaeva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-remove";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove user by login.";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = endpointLocator.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @Nullable final User user = endpointLocator.getUserEndpoint().removeUserByLogin(session, login);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}