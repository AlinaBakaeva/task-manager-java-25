package com.bakaeva.tm.command.data.base64;

import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.endpoint.DomainEndpoint;
import com.bakaeva.tm.endpoint.SessionDTO;
import com.bakaeva.tm.exception.security.AccessDeniedException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;



public final class DataBase64ClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-base64-clear";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove base64 binary data file.";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = endpointLocator.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[DATA BASE64 CLEAR]");
        @NotNull final DomainEndpoint domainEndpoint = endpointLocator.getDomainEndpoint();
        if (domainEndpoint.removeBase64(session)) System.out.println("[OK]");
    }

}