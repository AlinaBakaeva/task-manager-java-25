package com.bakaeva.tm.exception.system;

public class UnknownCommandException extends RuntimeException {

    public UnknownCommandException(String value) {
        super("Error! Unknown command: ``" + value + "``...");
    }

}