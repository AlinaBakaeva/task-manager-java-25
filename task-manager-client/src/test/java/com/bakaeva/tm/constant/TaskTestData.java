package com.bakaeva.tm.constant;

import com.bakaeva.tm.endpoint.Task;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.bakaeva.tm.constant.UserTestData.*;

@UtilityClass
public final class TaskTestData {

    @NotNull
    public final static Task USER1_TASK1 = new Task();

    @NotNull
    public final static Task USER1_TASK2 = new Task();

    @NotNull
    public final static Task USER1_TASK3 = new Task();

    @NotNull
    public final static Task USER2_TASK1 = new Task();

    @NotNull
    public final static Task ADMIN1_TASK1 = new Task();

    @NotNull
    public final static Task ADMIN1_TASK2 = new Task();

    @NotNull
    public final static List<Task> USER1_TASK_LIST = Arrays.asList(USER1_TASK1, USER1_TASK2, USER1_TASK3);

    @NotNull
    public final static List<Task> USER2_TASK_LIST = Collections.singletonList(USER2_TASK1);

    @NotNull
    public final static List<Task> ADMIN1_TASK_LIST = Arrays.asList(ADMIN1_TASK1, ADMIN1_TASK2);

    @NotNull
    public final static List<Task> TASK_LIST = new ArrayList<>();

    static {
        USER1_TASK_LIST.forEach(task -> task.setUserId(USER1.getId()));
        USER2_TASK_LIST.forEach(task -> task.setUserId(USER2.getId()));
        ADMIN1_TASK_LIST.forEach(task -> task.setUserId(ADMIN1.getId()));

        TASK_LIST.addAll(USER1_TASK_LIST);
        TASK_LIST.addAll(USER2_TASK_LIST);
        TASK_LIST.addAll(ADMIN1_TASK_LIST);

        for (int i = 0; i < TASK_LIST.size(); i++) {
            @NotNull final Task task = TASK_LIST.get(i);
            task.setId("t-0" + i);
            task.setName("task-" + i);
            task.setDescription("description of task-" + i);
        }
    }

}