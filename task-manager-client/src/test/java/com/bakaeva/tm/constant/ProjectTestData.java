package com.bakaeva.tm.constant;

import com.bakaeva.tm.endpoint.ProjectDTO;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.bakaeva.tm.constant.UserTestData.*;

@UtilityClass
public final class ProjectTestData {

    @NotNull
    public final static ProjectDTO USER1_PROJECT1 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO USER1_PROJECT2 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO USER1_PROJECT3 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO USER2_PROJECT1 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO ADMIN1_PROJECT1 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO ADMIN1_PROJECT2 = new ProjectDTO();

    @NotNull
    public final static List<ProjectDTO> USER1_PROJECT_LIST = Arrays.asList(USER1_PROJECT1, USER1_PROJECT2, USER1_PROJECT3);

    @NotNull
    public final static List<ProjectDTO> USER2_PROJECT_LIST = Collections.singletonList(USER2_PROJECT1);

    @NotNull
    public final static List<ProjectDTO> ADMIN1_PROJECT_LIST = Arrays.asList(ADMIN1_PROJECT1, ADMIN1_PROJECT2);

    @NotNull
    public final static List<ProjectDTO> PROJECT_LIST = new ArrayList<>();

    static {
        USER1_PROJECT_LIST.forEach(project -> project.setUserId(USER1.getId()));
        USER2_PROJECT_LIST.forEach(project -> project.setUserId(USER2.getId()));
        ADMIN1_PROJECT_LIST.forEach(project -> project.setUserId(ADMIN1.getId()));

        PROJECT_LIST.addAll(USER1_PROJECT_LIST);
        PROJECT_LIST.addAll(USER2_PROJECT_LIST);
        PROJECT_LIST.addAll(ADMIN1_PROJECT_LIST);

        for (int i = 0; i < PROJECT_LIST.size(); i++) {
            @NotNull final ProjectDTO project = PROJECT_LIST.get(i);
            project.setId("t-0" + i);
            project.setName("project-" + i);
            project.setDescription("description of project-" + i);
        }
    }

}
