package com.bakaeva.tm.util;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;

public class NumberUtilTest {

    @Test
    public void formatBytes() {
        final long gigabyte = 1073741824L;
        @NotNull final String result = NumberUtil.formatBytes(gigabyte);
        Assert.assertEquals("1 GB", result);
    }

}
