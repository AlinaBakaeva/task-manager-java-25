package com.bakaeva.tm.endpoint;

import com.bakaeva.tm.marker.IntegrationCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.bakaeva.tm.constant.ProjectTestData.*;
import static org.assertj.core.api.Assertions.assertThat;

@Category(IntegrationCategory.class)
public final class ProjectEndpointTest {

    @NotNull
    private final ProjectEndpoint endpoint = new ProjectEndpointService().getProjectEndpointPort();

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    private SessionDTO adminSession;

    private SessionDTO userSession;

    @Before
    public void setUp() {
        adminSession = sessionEndpoint.openSession("admin", "admin");
        endpoint.removeProjectAll(adminSession);
        userSession = sessionEndpoint.openSession("user", "user");
    }

    @After
    public void tearDown() {
        sessionEndpoint.closeSession(adminSession);
        sessionEndpoint.closeSession(userSession);
    }


    @Test
    public void createProject() {
        endpoint.createProject(userSession, USER1_PROJECT1.getName());
        Assert.assertNotNull(endpoint.findProjectByName(userSession, USER1_PROJECT1.getName()));
    }

    @Test(expected = Exception.class)
    public void createProjectNegative() {
        endpoint.createProject(null, USER1_PROJECT1.getName());
    }

    @Test
    public void createProjectWithDescription() {
        endpoint.createProjectWithDescription(userSession, USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription());
        @NotNull final ProjectDTO created = endpoint.findProjectByName(userSession, USER1_PROJECT1.getName());
        Assert.assertNotNull(created);
        Assert.assertEquals(USER1_PROJECT1.getDescription(), created.getDescription());
    }

    @Test
    public void addProject() {
        endpoint.addProject(userSession, USER1_PROJECT1);
        assertThat(endpoint.findProjectByName(userSession, USER1_PROJECT1.getName()))
                .isEqualToIgnoringGivenFields(USER1_PROJECT1, "userId");
    }

    @Test
    public void removeProject() {
        endpoint.addProject(userSession, USER1_PROJECT1);
        @NotNull final ProjectDTO project = endpoint.findProjectByName(userSession, USER1_PROJECT1.getName());
        Assert.assertNotNull(project);
        endpoint.removeProject(userSession, project);
        @NotNull final ProjectDTO tas2k = endpoint.findProjectByName(userSession, USER1_PROJECT1.getName());
    }

    @Test
    public void removeProjectAll() {
        endpoint.addProject(userSession, USER1_PROJECT1);
        endpoint.addProject(adminSession, ADMIN1_PROJECT1);
        Assert.assertFalse(endpoint.findProjectAll(userSession).isEmpty());
        Assert.assertFalse(endpoint.findProjectAll(adminSession).isEmpty());
        endpoint.removeProjectAll(adminSession);
        Assert.assertTrue(endpoint.findProjectAll(adminSession).isEmpty());
        Assert.assertTrue(endpoint.findProjectAll(userSession).isEmpty());
    }

    @Test
    public void removeProjectByUserAll() {
        endpoint.addProject(userSession, USER1_PROJECT1);
        endpoint.addProject(adminSession, ADMIN1_PROJECT1);
        Assert.assertFalse(endpoint.findProjectAll(userSession).isEmpty());
        Assert.assertFalse(endpoint.findProjectAll(adminSession).isEmpty());
        endpoint.removeProjectByUserAll(adminSession);
        Assert.assertTrue(endpoint.findProjectAll(adminSession).isEmpty());
        Assert.assertFalse(endpoint.findProjectAll(userSession).isEmpty());
        endpoint.removeProjectByUserAll(userSession);
        Assert.assertTrue(endpoint.findProjectAll(userSession).isEmpty());
    }

    @Test
    public void findProjectAll() {
        endpoint.addProject(userSession, USER1_PROJECT1);
        endpoint.addProject(adminSession, ADMIN1_PROJECT1);
        Assert.assertFalse(endpoint.findProjectAll(userSession).isEmpty());
        Assert.assertFalse(endpoint.findProjectAll(adminSession).isEmpty());
        endpoint.removeProjectByUserAll(adminSession);
        Assert.assertTrue(endpoint.findProjectAll(adminSession).isEmpty());
        Assert.assertFalse(endpoint.findProjectAll(userSession).isEmpty());
        endpoint.removeProjectByUserAll(userSession);
        Assert.assertTrue(endpoint.findProjectAll(userSession).isEmpty());
    }

    @Test
    public void findProjectById() {
        endpoint.addProject(userSession, USER1_PROJECT1);
        assertThat(endpoint.findProjectById(userSession, USER1_PROJECT1.getId()))
                .isEqualToIgnoringGivenFields(USER1_PROJECT1, "userId");
    }

    @Test
    public void findProjectByIndex() {
        endpoint.addProject(userSession, USER1_PROJECT1);
        endpoint.addProject(userSession, USER1_PROJECT2);
        assertThat(endpoint.findProjectByIndex(userSession, 1))
                .isEqualToIgnoringGivenFields(USER1_PROJECT2, "userId");

    }

    @Test
    public void findProjectByName() {
        endpoint.addProject(userSession, USER1_PROJECT1);
        assertThat(endpoint.findProjectByName(userSession, USER1_PROJECT1.getName()))
                .isEqualToIgnoringGivenFields(USER1_PROJECT1, "userId");
    }

    @Test
    public void removeProjectById() {
        endpoint.addProject(userSession, USER1_PROJECT1);
        assertThat(endpoint.findProjectByName(userSession, USER1_PROJECT1.getName()))
                .isEqualToIgnoringGivenFields(USER1_PROJECT1, "userId");
        assertThat(endpoint.removeProjectById(userSession, USER1_PROJECT1.getId()))
                .isEqualToIgnoringGivenFields(USER1_PROJECT1, "userId");
        Assert.assertNull(endpoint.findProjectByName(userSession, USER1_PROJECT1.getName()));
    }

    @Test
    public void removeProjectByIndex() {
        endpoint.addProject(userSession, USER1_PROJECT1);
        endpoint.addProject(userSession, USER1_PROJECT2);
        assertThat(endpoint.removeProjectByIndex(userSession, 1))
                .isEqualToIgnoringGivenFields(USER1_PROJECT2, "userId");
        Assert.assertNull(endpoint.findProjectByName(userSession, USER1_PROJECT2.getName()));
    }

    @Test
    public void removeProjectByName() {
        endpoint.addProject(userSession, USER1_PROJECT1);
        assertThat(endpoint.findProjectByName(userSession, USER1_PROJECT1.getName()))
                .isEqualToIgnoringGivenFields(USER1_PROJECT1, "userId");
        assertThat(endpoint.removeProjectByName(userSession, USER1_PROJECT1.getName()))
                .isEqualToIgnoringGivenFields(USER1_PROJECT1, "userId");
        Assert.assertNull(endpoint.findProjectByName(userSession, USER1_PROJECT1.getName()));
    }

    @Test
    public void updateProjectById() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("none");
        project.setDescription("none");
        project.setId(USER1_PROJECT1.getId());
        endpoint.addProject(userSession, project);
        endpoint.updateProjectById(
                userSession,
                project.getId(),
                USER1_PROJECT1.getName(),
                USER1_PROJECT1.getDescription()
        );
        assertThat(endpoint.findProjectById(userSession, project.getId()))
                .isEqualToIgnoringGivenFields(USER1_PROJECT1, "userId");
    }

    @Test
    public void updateProjectByIndex() {
        endpoint.addProject(userSession, USER1_PROJECT3);
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("none");
        project.setDescription("none");
        project.setId(USER1_PROJECT1.getId());
        endpoint.addProject(userSession, project);
        endpoint.updateProjectByIndex(
                userSession,
                1,
                USER1_PROJECT1.getName(),
                USER1_PROJECT1.getDescription()
        );
        assertThat(endpoint.findProjectById(userSession, project.getId()))
                .isEqualToIgnoringGivenFields(USER1_PROJECT1, "userId");
    }

}