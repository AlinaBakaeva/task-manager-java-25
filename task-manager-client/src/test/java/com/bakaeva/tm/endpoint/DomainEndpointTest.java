package com.bakaeva.tm.endpoint;

import com.bakaeva.tm.marker.IntegrationCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.List;

import static com.bakaeva.tm.constant.ProjectTestData.ADMIN1_PROJECT1;
import static com.bakaeva.tm.constant.TaskTestData.ADMIN1_TASK1;
import static com.bakaeva.tm.constant.UserTestData.ADMIN1;
import static org.assertj.core.api.Assertions.assertThat;

@Category(IntegrationCategory.class)
public final class DomainEndpointTest {

    @NotNull
    private final DomainEndpoint endpoint = new DomainEndpointService().getDomainEndpointPort();

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    private SessionDTO adminSession;

    @Before
    public void setUp() {
        adminSession = sessionEndpoint.openSession("admin", "admin");
        projectEndpoint.removeProjectAll(adminSession);
        taskEndpoint.removeTaskAll(adminSession);
    }

    @After
    public void tearDown() {
        userEndpoint.removeUserByLogin(adminSession, ADMIN1.login);
        sessionEndpoint.closeSessionAll(adminSession);
    }

    @Test(expected = Exception.class)
    public void saveToXmlNotAdminNegative() {
        @NotNull final SessionDTO userSession = sessionEndpoint.openSession("user", "user");
        endpoint.saveToXml(userSession);
    }

    @Test(expected = Exception.class)
    public void loadFromXmlNegative() {
        @NotNull final SessionDTO userSession = sessionEndpoint.openSession("user", "user");
        endpoint.loadFromXml(userSession);
    }

    @Test
    public void saveToXmlAndLoadFromXml() {
        projectEndpoint.addProject(adminSession, ADMIN1_PROJECT1);
        taskEndpoint.addTask(adminSession, ADMIN1_TASK1);
        userEndpoint.createUser(adminSession, ADMIN1.login, ADMIN1.login);
        @NotNull final List<Task> expectedTasks = taskEndpoint.findTaskAll(adminSession);
        @NotNull final List<ProjectDTO> expectedProjects = projectEndpoint.findProjectAll(adminSession);
        @NotNull final List<User> expectedUsers = userEndpoint.findUserAll(adminSession);
        endpoint.saveToXml(adminSession);

        projectEndpoint.removeProjectAll(adminSession);
        taskEndpoint.removeTaskAll(adminSession);
        userEndpoint.removeUserByLogin(adminSession, ADMIN1.getLogin());
        endpoint.loadFromXml(adminSession);
        adminSession = sessionEndpoint.openSession("admin", "admin");
        @NotNull final List<Task> actualTasks = taskEndpoint.findTaskAll(adminSession);
        @NotNull final List<ProjectDTO> actualProjects = projectEndpoint.findProjectAll(adminSession);
        @NotNull final List<User> actualUsers = userEndpoint.findUserAll(adminSession);
        for (int i = 0; i < actualProjects.size(); i++) {
            assertThat(actualProjects.get(i)).isEqualToComparingFieldByField(expectedProjects.get(i));
        }
        for (int i = 0; i < actualProjects.size(); i++) {
            assertThat(actualTasks.get(i)).isEqualToComparingFieldByField(expectedTasks.get(i));
        }
        for (int i = 0; i < actualProjects.size(); i++) {
            assertThat(actualUsers.get(i)).isEqualToComparingFieldByField(expectedUsers.get(i));
        }
    }

    @Test(expected = Exception.class)
    public void removeXmlNegative() {
        endpoint.saveToXml(adminSession);
        endpoint.removeXml(adminSession);
        endpoint.loadFromXml(adminSession);
    }

    @Test
    public void saveToJsonAndLoadFromJson() {
        projectEndpoint.addProject(adminSession, ADMIN1_PROJECT1);
        taskEndpoint.addTask(adminSession, ADMIN1_TASK1);
        userEndpoint.createUser(adminSession, ADMIN1.login, ADMIN1.login);
        @NotNull final List<Task> expectedTasks = taskEndpoint.findTaskAll(adminSession);
        @NotNull final List<ProjectDTO> expectedProjects = projectEndpoint.findProjectAll(adminSession);
        @NotNull final List<User> expectedUsers = userEndpoint.findUserAll(adminSession);
        endpoint.saveToJson(adminSession);

        projectEndpoint.removeProjectAll(adminSession);
        taskEndpoint.removeTaskAll(adminSession);
        userEndpoint.removeUserByLogin(adminSession, ADMIN1.getLogin());
        endpoint.loadFromJson(adminSession);
        adminSession = sessionEndpoint.openSession("admin", "admin");
        @NotNull final List<Task> actualTasks = taskEndpoint.findTaskAll(adminSession);
        @NotNull final List<ProjectDTO> actualProjects = projectEndpoint.findProjectAll(adminSession);
        @NotNull final List<User> actualUsers = userEndpoint.findUserAll(adminSession);
        for (int i = 0; i < actualProjects.size(); i++) {
            assertThat(actualProjects.get(i)).isEqualToComparingFieldByField(expectedProjects.get(i));
        }
        for (int i = 0; i < actualProjects.size(); i++) {
            assertThat(actualTasks.get(i)).isEqualToComparingFieldByField(expectedTasks.get(i));
        }
        for (int i = 0; i < actualProjects.size(); i++) {
            assertThat(actualUsers.get(i)).isEqualToComparingFieldByField(expectedUsers.get(i));
        }
    }

    @Test(expected = Exception.class)
    public void removeJsonNegative() {
        endpoint.saveToJson(adminSession);
        endpoint.removeJson(adminSession);
        endpoint.loadFromJson(adminSession);
    }

    @Test
    public void saveToBinaryAndLoadFromBinary() {
        projectEndpoint.addProject(adminSession, ADMIN1_PROJECT1);
        taskEndpoint.addTask(adminSession, ADMIN1_TASK1);
        userEndpoint.createUser(adminSession, ADMIN1.login, ADMIN1.login);
        @NotNull final List<Task> expectedTasks = taskEndpoint.findTaskAll(adminSession);
        @NotNull final List<ProjectDTO> expectedProjects = projectEndpoint.findProjectAll(adminSession);
        @NotNull final List<User> expectedUsers = userEndpoint.findUserAll(adminSession);
        endpoint.saveToBinary(adminSession);

        projectEndpoint.removeProjectAll(adminSession);
        taskEndpoint.removeTaskAll(adminSession);
        userEndpoint.removeUserByLogin(adminSession, ADMIN1.getLogin());
        endpoint.loadFromBinary(adminSession);
        adminSession = sessionEndpoint.openSession("admin", "admin");
        @NotNull final List<Task> actualTasks = taskEndpoint.findTaskAll(adminSession);
        @NotNull final List<ProjectDTO> actualProjects = projectEndpoint.findProjectAll(adminSession);
        @NotNull final List<User> actualUsers = userEndpoint.findUserAll(adminSession);
        for (int i = 0; i < actualProjects.size(); i++) {
            assertThat(actualProjects.get(i)).isEqualToComparingFieldByField(expectedProjects.get(i));
        }
        for (int i = 0; i < actualProjects.size(); i++) {
            assertThat(actualTasks.get(i)).isEqualToComparingFieldByField(expectedTasks.get(i));
        }
        for (int i = 0; i < actualProjects.size(); i++) {
            assertThat(actualUsers.get(i)).isEqualToComparingFieldByField(expectedUsers.get(i));
        }
    }

    @Test(expected = Exception.class)
    public void removeBinaryNegative() {
        endpoint.saveToBinary(adminSession);
        endpoint.removeBinary(adminSession);
        endpoint.loadFromBinary(adminSession);
    }

    @Test
    public void saveToBase64AndLoadFromBase64() {
        projectEndpoint.addProject(adminSession, ADMIN1_PROJECT1);
        taskEndpoint.addTask(adminSession, ADMIN1_TASK1);
        userEndpoint.createUser(adminSession, ADMIN1.login, ADMIN1.login);
        @NotNull final List<Task> expectedTasks = taskEndpoint.findTaskAll(adminSession);
        @NotNull final List<ProjectDTO> expectedProjects = projectEndpoint.findProjectAll(adminSession);
        @NotNull final List<User> expectedUsers = userEndpoint.findUserAll(adminSession);
        endpoint.saveToBase64(adminSession);

        projectEndpoint.removeProjectAll(adminSession);
        taskEndpoint.removeTaskAll(adminSession);
        userEndpoint.removeUserByLogin(adminSession, ADMIN1.getLogin());
        endpoint.loadFromBase64(adminSession);
        adminSession = sessionEndpoint.openSession("admin", "admin");
        @NotNull final List<Task> actualTasks = taskEndpoint.findTaskAll(adminSession);
        @NotNull final List<ProjectDTO> actualProjects = projectEndpoint.findProjectAll(adminSession);
        @NotNull final List<User> actualUsers = userEndpoint.findUserAll(adminSession);
        for (int i = 0; i < actualProjects.size(); i++) {
            assertThat(actualProjects.get(i)).isEqualToComparingFieldByField(expectedProjects.get(i));
        }
        for (int i = 0; i < actualProjects.size(); i++) {
            assertThat(actualTasks.get(i)).isEqualToComparingFieldByField(expectedTasks.get(i));
        }
        for (int i = 0; i < actualProjects.size(); i++) {
            assertThat(actualUsers.get(i)).isEqualToComparingFieldByField(expectedUsers.get(i));
        }
    }

    @Test(expected = Exception.class)
    public void removeBase64Negative() {
        endpoint.saveToBase64(adminSession);
        endpoint.removeBase64(adminSession);
        endpoint.loadFromBase64(adminSession);
    }

}