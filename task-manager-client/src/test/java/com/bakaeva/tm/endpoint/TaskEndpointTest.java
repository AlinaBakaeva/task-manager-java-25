package com.bakaeva.tm.endpoint;

import com.bakaeva.tm.marker.IntegrationCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.bakaeva.tm.constant.TaskTestData.*;
import static org.assertj.core.api.Assertions.assertThat;

@Category(IntegrationCategory.class)
public final class TaskEndpointTest {

    @NotNull
    private final TaskEndpoint endpoint = new TaskEndpointService().getTaskEndpointPort();

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    private SessionDTO adminSession;

    private SessionDTO userSession;

    @Before
    public void setUp() {
        adminSession = sessionEndpoint.openSession("admin", "admin");
        endpoint.removeTaskAll(adminSession);
        userSession = sessionEndpoint.openSession("user", "user");
    }

    @After
    public void tearDown() {
        sessionEndpoint.closeSession(adminSession);
        sessionEndpoint.closeSession(userSession);
    }


    @Test
    public void createTask() {
        endpoint.createTask(userSession, USER1_TASK1.getName());
        Assert.assertNotNull(endpoint.findTaskByName(userSession, USER1_TASK1.getName()));
    }

    @Test(expected = Exception.class)
    public void createTaskNegative() {
        endpoint.createTask(null, USER1_TASK1.getName());
    }

    @Test
    public void createTaskWithDescription() {
        endpoint.createTaskWithDescription(userSession, USER1_TASK1.getName(), USER1_TASK1.getDescription());
        @NotNull final Task created = endpoint.findTaskByName(userSession, USER1_TASK1.getName());
        Assert.assertNotNull(created);
        Assert.assertEquals(USER1_TASK1.getDescription(), created.getDescription());
    }

    @Test
    public void addTask() {
        endpoint.addTask(userSession, USER1_TASK1);
        assertThat(endpoint.findTaskByName(userSession, USER1_TASK1.getName()))
                .isEqualToIgnoringGivenFields(USER1_TASK1, "userId");
    }

    @Test
    public void removeTask() {
        endpoint.addTask(userSession, USER1_TASK1);
        @NotNull final Task task = endpoint.findTaskByName(userSession, USER1_TASK1.getName());
        Assert.assertNotNull(task);
        endpoint.removeTask(userSession, task);
        @NotNull final Task tas2k = endpoint.findTaskByName(userSession, USER1_TASK1.getName());
    }

    @Test
    public void removeTaskAll() {
        endpoint.addTask(userSession, USER1_TASK1);
        endpoint.addTask(adminSession, ADMIN1_TASK1);
        Assert.assertFalse(endpoint.findTaskAll(userSession).isEmpty());
        Assert.assertFalse(endpoint.findTaskAll(adminSession).isEmpty());
        endpoint.removeTaskAll(adminSession);
        Assert.assertTrue(endpoint.findTaskAll(adminSession).isEmpty());
        Assert.assertTrue(endpoint.findTaskAll(userSession).isEmpty());
    }

    @Test
    public void removeTaskByUserAll() {
        endpoint.addTask(userSession, USER1_TASK1);
        endpoint.addTask(adminSession, ADMIN1_TASK1);
        Assert.assertFalse(endpoint.findTaskAll(userSession).isEmpty());
        Assert.assertFalse(endpoint.findTaskAll(adminSession).isEmpty());
        endpoint.removeTaskByUserAll(adminSession);
        Assert.assertTrue(endpoint.findTaskAll(adminSession).isEmpty());
        Assert.assertFalse(endpoint.findTaskAll(userSession).isEmpty());
        endpoint.removeTaskByUserAll(userSession);
        Assert.assertTrue(endpoint.findTaskAll(userSession).isEmpty());
    }

    @Test
    public void findTaskAll() {
        endpoint.addTask(userSession, USER1_TASK1);
        endpoint.addTask(adminSession, ADMIN1_TASK1);
        Assert.assertFalse(endpoint.findTaskAll(userSession).isEmpty());
        Assert.assertFalse(endpoint.findTaskAll(adminSession).isEmpty());
        endpoint.removeTaskByUserAll(adminSession);
        Assert.assertTrue(endpoint.findTaskAll(adminSession).isEmpty());
        Assert.assertFalse(endpoint.findTaskAll(userSession).isEmpty());
        endpoint.removeTaskByUserAll(userSession);
        Assert.assertTrue(endpoint.findTaskAll(userSession).isEmpty());
    }

    @Test
    public void findTaskById() {
        endpoint.addTask(userSession, USER1_TASK1);
        assertThat(endpoint.findTaskById(userSession, USER1_TASK1.getId()))
                .isEqualToIgnoringGivenFields(USER1_TASK1, "userId");
    }

    @Test
    public void findTaskByIndex() {
        endpoint.addTask(userSession, USER1_TASK1);
        endpoint.addTask(userSession, USER1_TASK2);
        assertThat(endpoint.findTaskByIndex(userSession, 1))
                .isEqualToIgnoringGivenFields(USER1_TASK2, "userId");

    }

    @Test
    public void findTaskByName() {
        endpoint.addTask(userSession, USER1_TASK1);
        assertThat(endpoint.findTaskByName(userSession, USER1_TASK1.getName()))
                .isEqualToIgnoringGivenFields(USER1_TASK1, "userId");
    }

    @Test
    public void removeTaskById() {
        endpoint.addTask(userSession, USER1_TASK1);
        assertThat(endpoint.findTaskByName(userSession, USER1_TASK1.getName()))
                .isEqualToIgnoringGivenFields(USER1_TASK1, "userId");
        assertThat(endpoint.removeTaskById(userSession, USER1_TASK1.getId()))
                .isEqualToIgnoringGivenFields(USER1_TASK1, "userId");
        Assert.assertNull(endpoint.findTaskByName(userSession, USER1_TASK1.getName()));
    }

    @Test
    public void removeTaskByIndex() {
        endpoint.addTask(userSession, USER1_TASK1);
        endpoint.addTask(userSession, USER1_TASK2);
        assertThat(endpoint.removeTaskByIndex(userSession, 1))
                .isEqualToIgnoringGivenFields(USER1_TASK2, "userId");
        Assert.assertNull(endpoint.findTaskByName(userSession, USER1_TASK2.getName()));
    }

    @Test
    public void removeTaskByName() {
        endpoint.addTask(userSession, USER1_TASK1);
        assertThat(endpoint.findTaskByName(userSession, USER1_TASK1.getName()))
                .isEqualToIgnoringGivenFields(USER1_TASK1, "userId");
        assertThat(endpoint.removeTaskByName(userSession, USER1_TASK1.getName()))
                .isEqualToIgnoringGivenFields(USER1_TASK1, "userId");
        Assert.assertNull(endpoint.findTaskByName(userSession, USER1_TASK1.getName()));
    }

    @Test
    public void updateTaskById() {
        @NotNull final Task task = new Task();
        task.setName("none");
        task.setDescription("none");
        task.setId(USER1_TASK1.getId());
        endpoint.addTask(userSession, task);
        endpoint.updateTaskById(
                userSession,
                task.getId(),
                USER1_TASK1.getName(),
                USER1_TASK1.getDescription()
        );
        assertThat(endpoint.findTaskById(userSession, task.getId()))
                .isEqualToIgnoringGivenFields(USER1_TASK1, "userId");
    }

    @Test
    public void updateTaskByIndex() {
        endpoint.addTask(userSession, USER1_TASK3);
        @NotNull final Task task = new Task();
        task.setName("none");
        task.setDescription("none");
        task.setId(USER1_TASK1.getId());
        endpoint.addTask(userSession, task);
        endpoint.updateTaskByIndex(
                userSession,
                1,
                USER1_TASK1.getName(),
                USER1_TASK1.getDescription()
        );
        assertThat(endpoint.findTaskById(userSession, task.getId()))
                .isEqualToIgnoringGivenFields(USER1_TASK1, "userId");
    }

}
