package com.bakaeva.tm.endpoint;

import com.bakaeva.tm.marker.IntegrationCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.List;

import static com.bakaeva.tm.constant.UserTestData.USER1;
import static com.bakaeva.tm.constant.UserTestData.USER2;
import static org.assertj.core.api.Assertions.assertThat;

@Category(IntegrationCategory.class)
public final class UserEndpointTest {

    @NotNull
    private final UserEndpoint endpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @NotNull
    private final DomainEndpoint domainEndpoint = new DomainEndpointService().getDomainEndpointPort();

    private SessionDTO adminSession;

    private SessionDTO userSession;

    @Before
    public void setUp() {
        adminSession = sessionEndpoint.openSession("admin", "admin");
        userSession = sessionEndpoint.openSession("user", "user");
        domainEndpoint.saveToJson(adminSession);
    }

    @After
    public void tearDown() {
        domainEndpoint.loadFromJson(adminSession);
        adminSession = sessionEndpoint.openSession("admin", "admin");
        sessionEndpoint.closeSessionAll(adminSession);
    }

    @Test
    public void createUser() {
        @NotNull final User created = endpoint.createUser(adminSession, USER1.getLogin(), USER1.getLogin());
        Assert.assertEquals(USER1.getLogin(), created.getLogin());
        @NotNull final User actual = endpoint.findUserByLogin(adminSession, USER1.getLogin());
        Assert.assertEquals(USER1.getLogin(), actual.getLogin());
    }

    @Test(expected = Exception.class)
    public void createUserByNotAdminNegative() {
        @NotNull final User created = endpoint.createUser(userSession, USER1.getLogin(), USER1.getLogin());
    }

    @Test
    public void createUserWithEmail() {
        @NotNull final User created = endpoint.createUserWithEmail(
                adminSession, USER1.getLogin(), USER1.getLogin(), USER1.getEmail()
        );
        Assert.assertEquals(USER1.getLogin(), created.getLogin());
        Assert.assertEquals(USER1.getEmail(), created.getEmail());
        @NotNull final User actual = endpoint.findUserByLogin(adminSession, USER1.getLogin());
        Assert.assertEquals(USER1.getLogin(), actual.getLogin());
        Assert.assertEquals(USER1.getEmail(), created.getEmail());
    }

    @Test
    public void registerUserWithEmail() {
        @NotNull final User registered = endpoint.registerUserWithEmail(
                USER1.getLogin(), USER1.getLogin(), USER1.getEmail()
        );
        Assert.assertEquals(USER1.getLogin(), registered.getLogin());
        Assert.assertEquals(USER1.getEmail(), registered.getEmail());
        @NotNull final User actual = endpoint.findUserByLogin(adminSession, USER1.getLogin());
        Assert.assertEquals(USER1.getLogin(), actual.getLogin());
        Assert.assertEquals(USER1.getEmail(), registered.getEmail());
    }

    @Test
    public void createUserWithRole() {
        @NotNull final User created = endpoint.createUserWithRole(
                adminSession, USER1.getLogin(), USER1.getLogin(), USER1.getRole()
        );
        Assert.assertEquals(USER1.getLogin(), created.getLogin());
        Assert.assertEquals(USER1.getRole(), created.getRole());
        @NotNull final User actual = endpoint.findUserByLogin(adminSession, USER1.getLogin());
        Assert.assertEquals(USER1.getLogin(), actual.getLogin());
        Assert.assertEquals(USER1.getRole(), created.getRole());
    }

    @Test
    public void findUserById() {
        @NotNull final User expected = endpoint.findUserByLogin(adminSession, "admin");
        Assert.assertNotNull(expected);
        @NotNull final User actual = endpoint.findUserById(adminSession, expected.getId());
        Assert.assertNotNull(actual);
        assertThat(actual).isEqualToComparingFieldByField(expected);
    }

    @Test
    public void findUserByLogin() {
        @NotNull final User created = endpoint.createUser(adminSession, USER1.getLogin(), USER1.getLogin());
        Assert.assertEquals(USER1.getLogin(), created.getLogin());
        @NotNull final User actual = endpoint.findUserByLogin(adminSession, USER1.getLogin());
        Assert.assertEquals(USER1.getLogin(), actual.getLogin());
    }

    @Test
    public void removeUserById() {
        @NotNull final User expected = endpoint.findUserByLogin(adminSession, "user");
        Assert.assertNotNull(expected);
        @NotNull final User removed = endpoint.removeUserById(adminSession, expected.getId());
        Assert.assertEquals(expected.getId(), removed.getId());
        Assert.assertNull(endpoint.findUserById(adminSession, removed.getLogin()));
    }

    @Test
    public void removeUserByLogin() {
        @NotNull final User expected = endpoint.findUserByLogin(adminSession, "user");
        Assert.assertNotNull(expected);
        @NotNull final User removed = endpoint.removeUserByLogin(adminSession, expected.getLogin());
        Assert.assertEquals(expected.getLogin(), removed.getLogin());
        Assert.assertNull(endpoint.findUserById(adminSession, removed.getLogin()));
    }

    @Test
    public void updateUser() {
        @NotNull final User created = endpoint.createUser(adminSession, USER1.getLogin(), USER1.getLogin());
        @NotNull final SessionDTO newUserSession = sessionEndpoint.openSession(USER1.getLogin(), USER1.getLogin());
        Assert.assertNotNull(endpoint.updateUser(
                newUserSession,
                USER2.getLogin(),
                USER2.getFirstName(),
                USER2.getLastName(),
                USER2.getMiddleName(),
                USER2.getEmail()
        ));
        @NotNull final User updated = endpoint.findUserById(adminSession, created.getId());
        assertThat(updated).isEqualToIgnoringGivenFields(USER2, "id", "passwordHash", "locked");
    }

    @Test
    public void updateUserById() {
        @NotNull final User created = endpoint.createUser(adminSession, USER1.getLogin(), USER1.getLogin());
        @NotNull final SessionDTO newUserSession = sessionEndpoint.openSession(USER1.getLogin(), USER1.getLogin());
        Assert.assertNotNull(endpoint.updateUserById(
                adminSession,
                created.getId(),
                USER2.getLogin(),
                USER2.getFirstName(),
                USER2.getLastName(),
                USER2.getMiddleName(),
                USER2.getEmail()
        ));
        @NotNull final User updated = endpoint.findUserById(adminSession, created.getId());
        assertThat(updated).isEqualToIgnoringGivenFields(USER2, "id", "passwordHash", "locked");
    }

    @Test
    public void updateUserPassword() {
        @NotNull final String password = USER1.getLogin();
        endpoint.createUser(adminSession, USER1.getLogin(), password);
        @NotNull final SessionDTO newUserSession = sessionEndpoint.openSession(USER1.getLogin(), password);
        @NotNull final String passwordNew = USER2.getLogin();
        Assert.assertNotNull(endpoint.updateUserPassword(newUserSession, passwordNew));
        sessionEndpoint.closeSession(newUserSession);
        Assert.assertNotNull(sessionEndpoint.openSession(USER1.getLogin(), passwordNew));
    }

    @Test
    public void updateUserPasswordById() {
        @NotNull final String password = USER1.getLogin();
        @NotNull final User created = endpoint.createUser(adminSession, USER1.getLogin(), password);
        @NotNull final String passwordNew = USER2.getLogin();
        Assert.assertNotNull(endpoint.updateUserPasswordById(adminSession, created.getId(), passwordNew));
        Assert.assertNotNull(sessionEndpoint.openSession(USER1.getLogin(), passwordNew));
    }

    @Test
    public void lockUserByLogin() {
        @NotNull final String password = USER1.getLogin();
        @NotNull final User created = endpoint.createUser(adminSession, USER1.getLogin(), password);
        @NotNull final User locked = endpoint.lockUserByLogin(adminSession, USER1.getLogin());
        Assert.assertNull(sessionEndpoint.openSession(USER1.getLogin(), password));
    }

    @Test
    public void unlockUserByLogin() {
        @NotNull final String password = USER1.getLogin();
        @NotNull final User created = endpoint.createUser(adminSession, USER1.getLogin(), password);
        @NotNull final User locked = endpoint.lockUserByLogin(adminSession, USER1.getLogin());
        Assert.assertNull(sessionEndpoint.openSession(USER1.getLogin(), password));
        Assert.assertNotNull(endpoint.unlockUserByLogin(adminSession, USER1.getLogin()));
    }

    @Test
    public void findUserAll() {
        @NotNull final List<User> actual = endpoint.findUserAll(adminSession);
        actual.forEach(user -> assertThat(user).isInstanceOf(User.class));
        Assert.assertEquals(2, actual.size());
    }

    @Test
    public void removeUser() {
        @NotNull final User created = endpoint.createUser(adminSession, USER1.getLogin(), USER1.getLogin());
        Assert.assertNotNull(endpoint.findUserById(adminSession, created.getId()));
        assertThat(endpoint.removeUser(adminSession, created)).isInstanceOf(User.class);
        Assert.assertNull(endpoint.findUserById(adminSession, created.getId()));
    }

}